package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.service.IDataInterChangeService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IDomainService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IConfigurationService;
import ru.renessans.jvschool.volkov.task.manager.dto.Domain;
import ru.renessans.jvschool.volkov.task.manager.util.DataMarshalizerUtil;
import ru.renessans.jvschool.volkov.task.manager.util.DataSerializerUtil;
import ru.renessans.jvschool.volkov.task.manager.util.FileUtil;

@AllArgsConstructor
public final class DataInterChangeService implements IDataInterChangeService {

    @NotNull
    private final IConfigurationService configService;

    @NotNull
    private final IDomainService domainService;

    @Override
    public boolean dataBinClear() {
        @NotNull final String locate = this.configService.getBinFileName();
        return FileUtil.delete(locate);
    }

    @Override
    public boolean dataBase64Clear() {
        @NotNull final String locate = this.configService.getBase64FileName();
        return FileUtil.delete(locate);
    }

    @Override
    public boolean dataJsonClear() {
        @NotNull final String locate = this.configService.getJsonFileName();
        return FileUtil.delete(locate);
    }

    @Override
    public boolean dataXmlClear() {
        @NotNull final String locate = this.configService.getXmlFileName();
        return FileUtil.delete(locate);
    }

    @Override
    public boolean dataYamlClear() {
        @NotNull final String locate = this.configService.getYamlFileName();
        return FileUtil.delete(locate);
    }

    @NotNull
    @SneakyThrows
    @Override
    public Domain exportDataBin() {
        @NotNull final Domain domain = new Domain();
        this.domainService.dataExport(domain);

        @NotNull final String locate = this.configService.getBinFileName();
        try (@Nullable final DataSerializerUtil dataInterchange = new DataSerializerUtil()) {
            dataInterchange.writeToBin(domain, locate);
        }

        return domain;
    }

    @NotNull
    @SneakyThrows
    @Override
    public Domain exportDataBase64() {
        @NotNull final Domain domain = new Domain();
        this.domainService.dataExport(domain);

        @NotNull final String locate = this.configService.getBase64FileName();
        try (@Nullable final DataSerializerUtil dataInterchange = new DataSerializerUtil()) {
            dataInterchange.writeToBase64(domain, locate);
        }

        return domain;
    }

    @NotNull
    @Override
    public Domain exportDataJson() {
        @NotNull final Domain domain = new Domain();
        this.domainService.dataExport(domain);

        @NotNull final String locate = this.configService.getJsonFileName();
        DataMarshalizerUtil.writeToJson(domain, locate);

        return domain;
    }

    @NotNull
    @Override
    public Domain exportDataXml() {
        @NotNull final Domain domain = new Domain();
        this.domainService.dataExport(domain);

        @NotNull final String locate = this.configService.getXmlFileName();
        DataMarshalizerUtil.writeToXml(domain, locate);

        return domain;
    }

    @NotNull
    @SneakyThrows
    @Override
    public Domain exportDataYaml() {
        @NotNull final Domain domain = new Domain();
        this.domainService.dataExport(domain);

        @NotNull final String locate = this.configService.getYamlFileName();
        DataMarshalizerUtil.writeToYaml(domain, locate);

        return domain;
    }

    @NotNull
    @SneakyThrows
    @Override
    public Domain importDataBin() {
        @NotNull final String locate = this.configService.getBinFileName();
        @Nullable final Domain domain;

        try (@NotNull final DataSerializerUtil dataInterchange = new DataSerializerUtil()) {
            domain = dataInterchange.readFromBin(locate, Domain.class);
        }

        this.domainService.dataImport(domain);
        return domain;
    }

    @NotNull
    @SneakyThrows
    @Override
    public Domain importDataBase64() {
        @NotNull final String locate = this.configService.getBase64FileName();
        @Nullable final Domain domain;

        try (@NotNull final DataSerializerUtil dataInterchange = new DataSerializerUtil()) {
            domain = dataInterchange.readFromBase64(locate, Domain.class);
        }

        this.domainService.dataImport(domain);
        return domain;
    }

    @NotNull
    @Override
    public Domain importDataJson() {
        @NotNull final String locate = this.configService.getJsonFileName();
        @NotNull final Domain domain = DataMarshalizerUtil.readFromJson(locate, Domain.class);
        this.domainService.dataImport(domain);
        return domain;
    }

    @NotNull
    @Override
    public Domain importDataXml() {
        @NotNull final String locate = this.configService.getXmlFileName();
        @NotNull final Domain domain = DataMarshalizerUtil.readFromXml(locate, Domain.class);
        this.domainService.dataImport(domain);
        return domain;
    }

    @NotNull
    @Override
    public Domain importDataYaml() {
        @NotNull final String locate = this.configService.getYamlFileName();
        @NotNull final Domain domain = DataMarshalizerUtil.readFromYaml(locate, Domain.class);
        this.domainService.dataImport(domain);
        return domain;
    }

}