package ru.renessans.jvschool.volkov.task.manager.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.IServiceLocatorService;
import ru.renessans.jvschool.volkov.task.manager.api.endpoint.ITaskEndpoint;
import ru.renessans.jvschool.volkov.task.manager.api.service.ISessionService;
import ru.renessans.jvschool.volkov.task.manager.api.service.ITaskUserService;
import ru.renessans.jvschool.volkov.task.manager.model.Session;
import ru.renessans.jvschool.volkov.task.manager.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import java.util.Collection;

@WebService
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint() {
    }

    public TaskEndpoint(
            @NotNull final IServiceLocatorService serviceLocator
    ) {
        super(serviceLocator);
    }

    @WebMethod
    @WebResult(name = "task", partName = "task")
    @NotNull
    @SneakyThrows
    @Override
    public Task addTask(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "title", partName = "title") @Nullable final String title,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session);
        @NotNull final ITaskUserService taskService = super.serviceLocator.getTaskService();
        return taskService.add(opened.getUserId(), title, description);
    }

    @WebMethod
    @WebResult(name = "updatedTask", partName = "updatedTask")
    @Nullable
    @SneakyThrows
    @Override
    public Task updateTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "title", partName = "title") @Nullable final String title,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session);
        @NotNull final ITaskUserService taskService = super.serviceLocator.getTaskService();
        return taskService.updateById(opened.getUserId(), id, title, description);
    }

    @WebMethod
    @WebResult(name = "updatedTask", partName = "updatedTask")
    @Nullable
    @SneakyThrows
    @Override
    public Task updateTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index,
            @WebParam(name = "title", partName = "title") @Nullable final String title,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session);
        @NotNull final ITaskUserService taskService = super.serviceLocator.getTaskService();
        return taskService.updateByIndex(opened.getUserId(), index, title, description);
    }

    @WebMethod
    @WebResult(name = "deletedTask", partName = "deletedTask")
    @Nullable
    @SneakyThrows
    @Override
    public Task deleteTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session);
        @NotNull final ITaskUserService taskService = super.serviceLocator.getTaskService();
        return taskService.deleteById(opened.getUserId(), id);
    }

    @WebMethod
    @WebResult(name = "deletedTask", partName = "deletedTask")
    @Nullable
    @SneakyThrows
    @Override
    public Task deleteTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session);
        @NotNull final ITaskUserService taskService = super.serviceLocator.getTaskService();
        return taskService.deleteByIndex(opened.getUserId(), index);
    }

    @WebMethod
    @WebResult(name = "deletedTask", partName = "deletedTask")
    @Nullable
    @SneakyThrows
    @Override
    public Task deleteTaskByTitle(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "title", partName = "title") @Nullable final String title
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session);
        @NotNull final ITaskUserService taskService = super.serviceLocator.getTaskService();
        return taskService.deleteByTitle(opened.getUserId(), title);
    }

    @WebMethod
    @WebResult(name = "deletedTasks", partName = "deletedTasks")
    @NotNull
    @SneakyThrows
    @Override
    public Collection<Task> deleteAllTasks(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session);
        @NotNull final ITaskUserService taskService = super.serviceLocator.getTaskService();
        return taskService.deleteAll(opened.getUserId());
    }

    @WebMethod
    @WebResult(name = "task", partName = "task")
    @Nullable
    @SneakyThrows
    @Override
    public Task getTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session);
        @NotNull final ITaskUserService taskService = super.serviceLocator.getTaskService();
        return taskService.getById(opened.getUserId(), id);
    }

    @WebMethod
    @WebResult(name = "task", partName = "task")
    @Nullable
    @SneakyThrows
    @Override
    public Task getTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session);
        @NotNull final ITaskUserService taskService = super.serviceLocator.getTaskService();
        return taskService.getByIndex(opened.getUserId(), index);
    }

    @WebMethod
    @WebResult(name = "task", partName = "task")
    @Nullable
    @SneakyThrows
    @Override
    public Task getTaskByTitle(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "title", partName = "title") @Nullable final String title
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session);
        @NotNull final ITaskUserService taskService = super.serviceLocator.getTaskService();
        return taskService.getByTitle(opened.getUserId(), title);
    }

    @WebMethod
    @WebResult(name = "tasks", partName = "tasks")
    @NotNull
    @SneakyThrows
    @Override
    public Collection<Task> getAllTasks(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session);
        @NotNull final ITaskUserService taskService = super.serviceLocator.getTaskService();
        return taskService.getAll(opened.getUserId());
    }

}