package ru.renessans.jvschool.volkov.task.manager.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.repository.ISessionRepository;
import ru.renessans.jvschool.volkov.task.manager.model.Session;

import java.util.Collection;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;

public final class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    @Override
    public boolean containsUserId(@NotNull final String userId) {
        @NotNull final Collection<Session> allSessions = super.getAllRecords();
        @NotNull final AtomicBoolean isContains = new AtomicBoolean(false);
        allSessions.forEach(session -> {
            if (userId.equals(session.getUserId())) isContains.set(true);
        });
        return isContains.get();
    }

    @Nullable
    @Override
    public Session getSessionByUserId(@NotNull final String userId) {
        @NotNull final Collection<Session> allData = super.getAllRecords();
        return allData
                .stream()
                .filter(session -> userId.equals(session.getUserId()))
                .findAny()
                .orElse(null);
    }

    @SneakyThrows
    @Override
    public boolean deleteByUserId(@NotNull final String userId) {
        @Nullable final Session userSession = getSessionByUserId(userId);
        if (Objects.isNull(userSession)) return false;
        return super.deletedRecord(userSession);
    }

}