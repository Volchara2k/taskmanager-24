package ru.renessans.jvschool.volkov.task.manager.util;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.exception.empty.file.EmptyFilenameException;
import ru.renessans.jvschool.volkov.task.manager.exception.empty.owner.EmptyFileException;
import ru.renessans.jvschool.volkov.task.manager.exception.empty.service.EmptyKeyException;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

public final class DataSerializerUtil implements Closeable {

    @Nullable
    private ObjectInputStream objectInputStream;

    @Nullable
    private ObjectOutputStream objectOutputStream;

    @Nullable
    private ByteArrayInputStream byteArrayInputStream;

    @Nullable
    private ByteArrayOutputStream byteArrayOutputStream;

    @Nullable
    private FileInputStream fileInputStream;

    @Nullable
    private FileOutputStream fileOutputStream;

    @Override
    public void close() throws IOException {
        if (!Objects.isNull(this.objectOutputStream)) this.objectOutputStream.close();
        if (!Objects.isNull(this.objectInputStream)) this.objectInputStream.close();
        if (!Objects.isNull(this.byteArrayOutputStream)) this.byteArrayOutputStream.close();
        if (!Objects.isNull(this.byteArrayInputStream)) this.byteArrayInputStream.close();
        if (!Objects.isNull(this.fileOutputStream)) this.fileOutputStream.close();
        if (!Objects.isNull(this.fileInputStream)) this.fileInputStream.close();
    }

    @NotNull
    @SneakyThrows
    public <T extends Serializable> T writeToBin(
            @Nullable final T t,
            @Nullable final String filename
    ) {
        if (Objects.isNull(t)) throw new EmptyKeyException();
        if (ValidRuleUtil.isNullOrEmpty(filename)) throw new EmptyFilenameException();

        @NotNull final File file = FileUtil.create(filename);
        this.fileOutputStream = new FileOutputStream(file);
        this.objectOutputStream = new ObjectOutputStream(this.fileOutputStream);
        this.objectOutputStream.writeObject(t);
        this.objectOutputStream.flush();

        return t;
    }

    @NotNull
    @SneakyThrows
    public <T extends Serializable> T readFromBin(
            @Nullable final String filename,
            @Nullable final Class<T> tClass
    ) {
        if (ValidRuleUtil.isNullOrEmpty(filename)) throw new EmptyFilenameException();
        if (Objects.isNull(tClass)) throw new EmptyKeyException();
        if (ValidRuleUtil.isNullOrEmpty(filename)) throw new EmptyFilenameException();

        if (FileUtil.isNotExists(filename)) throw new EmptyFileException();
        this.fileInputStream = new FileInputStream(filename);
        this.objectInputStream = new ObjectInputStream(this.fileInputStream);

        return tClass.cast(this.objectInputStream.readObject());
    }

    @NotNull
    @SneakyThrows
    public <T extends Serializable> T writeToBase64(
            @Nullable final T t,
            @Nullable final String filename
    ) {
        if (Objects.isNull(t)) throw new EmptyKeyException();
        if (ValidRuleUtil.isNullOrEmpty(filename)) throw new EmptyFilenameException();

        this.byteArrayOutputStream = new ByteArrayOutputStream();
        this.objectOutputStream = new ObjectOutputStream(this.byteArrayOutputStream);
        this.objectOutputStream.writeObject(t);
        this.objectOutputStream.flush();

        final byte[] fileBytes = byteArrayOutputStream.toByteArray();
        @NotNull final String base64 = new BASE64Encoder().encode(fileBytes);
        final byte[] base64Bytes = base64.getBytes(StandardCharsets.UTF_8);

        @NotNull final File file = FileUtil.create(filename);
        this.fileOutputStream = new FileOutputStream(file.getName());
        this.fileOutputStream.write(base64Bytes);
        this.fileOutputStream.flush();

        return t;
    }

    @NotNull
    @SneakyThrows
    public <T extends Serializable> T readFromBase64(
            @Nullable final String filename,
            @Nullable final Class<T> tClass
    ) {
        if (ValidRuleUtil.isNullOrEmpty(filename)) throw new EmptyFilenameException();
        if (Objects.isNull(tClass)) throw new EmptyKeyException();
        if (FileUtil.isNotExists(filename)) throw new EmptyFileException();

        final byte[] fileBytes = FileUtil.read(filename);
        @NotNull final String base64 = new String(fileBytes);
        final byte[] base64Bytes = new BASE64Decoder().decodeBuffer(base64);

        this.byteArrayInputStream = new ByteArrayInputStream(base64Bytes);
        this.objectInputStream = new ObjectInputStream(this.byteArrayInputStream);

        return tClass.cast(this.objectInputStream.readObject());
    }

}