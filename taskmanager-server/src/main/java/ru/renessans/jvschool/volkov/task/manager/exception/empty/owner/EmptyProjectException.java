package ru.renessans.jvschool.volkov.task.manager.exception.empty.owner;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class EmptyProjectException extends AbstractException {

    @NotNull
    private static final String EMPTY_PROJECT = "Ошибка! Параметр \"проект\" является null!\n";

    public EmptyProjectException() {
        super(EMPTY_PROJECT);
    }

}