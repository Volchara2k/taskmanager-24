package ru.renessans.jvschool.volkov.task.manager.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.model.Project;
import ru.renessans.jvschool.volkov.task.manager.model.Task;
import ru.renessans.jvschool.volkov.task.manager.model.User;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public final class Domain implements Serializable {

    @NotNull
    private Collection<Project> projects = new ArrayList<>();

    @NotNull
    private Collection<Task> tasks = new ArrayList<>();

    @NotNull
    private Collection<User> users = new ArrayList<>();

}