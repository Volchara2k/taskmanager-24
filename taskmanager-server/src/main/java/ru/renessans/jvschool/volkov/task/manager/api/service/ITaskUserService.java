package ru.renessans.jvschool.volkov.task.manager.api.service;

import ru.renessans.jvschool.volkov.task.manager.model.Task;

public interface ITaskUserService extends IOwnerUserService<Task> {
}