package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.service.IDomainService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IProjectUserService;
import ru.renessans.jvschool.volkov.task.manager.api.service.ITaskUserService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserService;
import ru.renessans.jvschool.volkov.task.manager.dto.Domain;
import ru.renessans.jvschool.volkov.task.manager.exception.empty.owner.EmptyDomainException;

import java.util.Objects;

@AllArgsConstructor
public final class DomainService implements IDomainService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final ITaskUserService taskService;

    @NotNull
    private final IProjectUserService projectService;

    @SneakyThrows
    @Override
    public Domain dataImport(@Nullable final Domain domain) {
        if (Objects.isNull(domain)) throw new EmptyDomainException();
        this.userService.setAllRecords(domain.getUsers());
        this.taskService.setAllRecords(domain.getTasks());
        this.projectService.setAllRecords(domain.getProjects());
        return domain;
    }

    @SneakyThrows
    @Override
    public Domain dataExport(@Nullable final Domain domain) {
        if (Objects.isNull(domain)) throw new EmptyDomainException();
        domain.setUsers(this.userService.getAllRecords());
        domain.setTasks(this.taskService.getAllRecords());
        domain.setProjects(this.projectService.getAllRecords());
        return domain;
    }

}