package ru.renessans.jvschool.volkov.task.manager.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.dto.Domain;
import ru.renessans.jvschool.volkov.task.manager.model.Session;

public interface IAdminDataInterChangeEndpoint {

    boolean dataBinClear(@Nullable final Session session);

    boolean dataBase64Clear(@Nullable final Session session);

    boolean dataJsonClear(@Nullable final Session session);

    boolean dataXmlClear(@Nullable final Session session);

    boolean dataYamlClear(@Nullable final Session session);

    @NotNull
    Domain exportDataBin(@Nullable final Session session);

    @NotNull
    Domain exportDataBase64(@Nullable final Session session);

    @NotNull
    Domain exportDataJson(@Nullable final Session session);

    @NotNull
    Domain exportDataXml(@Nullable final Session session);

    @NotNull
    Domain exportDataYaml(@Nullable final Session session);

    @NotNull
    Domain importDataBin(@Nullable final Session session);

    @NotNull
    Domain importDataBase64(@Nullable final Session session);

    @NotNull
    Domain importDataJson(@Nullable final Session session);

    @NotNull
    Domain importDataXml(@Nullable final Session session);

    @NotNull
    Domain importDataYaml(@Nullable final Session session);

}