package ru.renessans.jvschool.volkov.task.manager.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRole;
import ru.renessans.jvschool.volkov.task.manager.model.Project;
import ru.renessans.jvschool.volkov.task.manager.model.Session;
import ru.renessans.jvschool.volkov.task.manager.model.Task;
import ru.renessans.jvschool.volkov.task.manager.model.User;

import java.util.Collection;

public interface IAdminEndpoint {

    boolean closeAllSessions(
            Session session
    );

    @NotNull
    Collection<Session> getAllSessions(
            @Nullable Session session
    );

    @NotNull
    User signUpUserWithUserRole(
            @Nullable Session session,
            @Nullable String login,
            @Nullable String password,
            @Nullable UserRole role
    );

    @Nullable
    User deleteUserById(
            @Nullable Session session,
            @Nullable String id
    );

    @Nullable
    User deleteUserByLogin(
            @Nullable Session session,
            @Nullable String login
    );

    @NotNull
    public Collection<User> deleteAllUsers(
            @Nullable Session session
    );

    @Nullable
    User lockUserByLogin(
            @Nullable Session session,
            @Nullable String login
    );

    @Nullable
    User unlockUserByLogin(
            @Nullable Session session,
            @Nullable String login
    );

    @NotNull
    Collection<User> setAllUsers(
            @Nullable Session session,
            @Nullable Collection<User> values
    );

    @NotNull
    Collection<User> getAllUsers(
            @Nullable Session session
    );

    @NotNull
    Collection<Task> getAllUsersTasks(
            @Nullable Session session
    );

    @NotNull
    Collection<Task> setAllUsersTasks(
            @Nullable Session session,
            @Nullable Collection<Task> values
    );

    @NotNull
    Collection<Project> getAllUsersProjects(
            @Nullable Session session
    );

    @NotNull
    Collection<Project> setAllUsersProjects(
            @Nullable Session session,
            @Nullable Collection<Project> values
    );

    @Nullable
    User getUserById(
            @Nullable Session session,
            @Nullable String id
    );

    @Nullable
    User getUserByLogin(
            @Nullable Session session,
            @Nullable String login
    );

    @Nullable
    User editProfileById(
            @Nullable Session session,
            @Nullable String id,
            @Nullable String firstName
    );

    @Nullable
    User editProfileByIdWithLastName(
            @Nullable Session session,
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String lastName
    );

    @Nullable
    User updatePasswordById(
            @Nullable Session session,
            @Nullable String id,
            @Nullable String newPassword
    );

}