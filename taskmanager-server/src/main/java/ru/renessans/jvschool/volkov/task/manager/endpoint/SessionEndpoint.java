package ru.renessans.jvschool.volkov.task.manager.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.IServiceLocatorService;
import ru.renessans.jvschool.volkov.task.manager.api.endpoint.ISessionEndpoint;
import ru.renessans.jvschool.volkov.task.manager.api.service.ISessionService;
import ru.renessans.jvschool.volkov.task.manager.enumeration.PermissionValidState;
import ru.renessans.jvschool.volkov.task.manager.enumeration.SessionValidState;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRole;
import ru.renessans.jvschool.volkov.task.manager.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

@WebService
public final class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    public SessionEndpoint() {
    }

    public SessionEndpoint(
            @NotNull final IServiceLocatorService serviceLocator
    ) {
        super(serviceLocator);
    }

    @WebMethod
    @WebResult(name = "openedSession", partName = "openedSession")
    @NotNull
    @SneakyThrows
    @Override
    public Session openSession(
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        return sessionService.openSession(login, password);
    }

    @WebMethod
    @WebResult(name = "isClosed", partName = "isClosed")
    @SneakyThrows
    @Override
    public boolean closeSession(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        return sessionService.closeSession(session);
    }

    @WebMethod
    @WebResult(name = "session", partName = "session")
    @NotNull
    @SneakyThrows
    @Override
    public Session validateSession(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session);
        return opened;
    }

    @WebMethod
    @WebResult(name = "session", partName = "session")
    @NotNull
    @SneakyThrows
    @Override
    public Session validateSessionWithCommandRole(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "commandRole", partName = "commandRole") @Nullable final UserRole command
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session, command);
        return opened;
    }

    @WebMethod
    @WebResult(name = "sessionValidState", partName = "sessionValidState")
    @NotNull
    @SneakyThrows
    @Override
    public SessionValidState verifyValidSessionState(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session);
        return sessionService.verifyValidSessionState(opened);
    }

    @WebMethod
    @WebResult(name = "permissionValidState", partName = "permissionValidState")
    @NotNull
    @SneakyThrows
    @Override
    public PermissionValidState verifyValidPermissionState(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "commandRoles", partName = "commandRoles") @Nullable final UserRole commandRole
    ) {
        @NotNull final ISessionService sessionService = super.serviceLocator.getSessionService();
        @NotNull final Session opened = sessionService.validateSession(session, commandRole);
        return sessionService.verifyValidPermissionState(opened, commandRole);
    }

}