package ru.renessans.jvschool.volkov.task.manager.exception.empty.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class EmptyHostException extends AbstractException {

    @NotNull
    private static final String EMPTY_HOST = "Ошибка! Параметр \"host\" является пустым или null!\n";

    public EmptyHostException() {
        super(EMPTY_HOST);
    }

}