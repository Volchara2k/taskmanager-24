package ru.renessans.jvschool.volkov.task.manager.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.enumeration.PermissionValidState;
import ru.renessans.jvschool.volkov.task.manager.enumeration.SessionValidState;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRole;
import ru.renessans.jvschool.volkov.task.manager.model.Session;

import javax.jws.WebMethod;

public interface ISessionEndpoint {

    @NotNull
    Session openSession(
            @Nullable String login,
            @Nullable String password
    );

    boolean closeSession(
            @Nullable Session session
    );

    @NotNull
    Session validateSession(
            @Nullable Session session
    );

    @NotNull
    @WebMethod
    Session validateSessionWithCommandRole(
            @Nullable Session session,
            @Nullable UserRole commandRole
    );

    @NotNull
    SessionValidState verifyValidSessionState(
            @Nullable Session session
    );

    @NotNull
    PermissionValidState verifyValidPermissionState(
            @Nullable Session session,
            @Nullable UserRole commandRole
    );

}