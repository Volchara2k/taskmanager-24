package ru.renessans.jvschool.volkov.task.manager.exception.empty.user;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class EmptyPasswordException extends AbstractException {

    @NotNull
    private static final String EMPTY_PASSWORD = "Ошибка! Параметр \"пароль\" является пустым или null!\n";

    public EmptyPasswordException() {
        super(EMPTY_PASSWORD);
    }

}