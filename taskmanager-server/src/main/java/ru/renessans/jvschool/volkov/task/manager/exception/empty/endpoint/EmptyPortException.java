package ru.renessans.jvschool.volkov.task.manager.exception.empty.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class EmptyPortException extends AbstractException {

    @NotNull
    private static final String EMPTY_PORT = "Ошибка! Параметр \"port\" является пустым или null!\n";

    public EmptyPortException() {
        super(EMPTY_PORT);
    }

}