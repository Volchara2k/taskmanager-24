package ru.renessans.jvschool.volkov.task.manager.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.IRepository;
import ru.renessans.jvschool.volkov.task.manager.model.Session;

public interface ISessionRepository extends IRepository<Session> {

    boolean containsUserId(@NotNull final String userId);

    @Nullable
    Session getSessionByUserId(@NotNull String userId);

    boolean deleteByUserId(@NotNull String userId);

}