package ru.renessans.jvschool.volkov.task.manager.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.model.Session;
import ru.renessans.jvschool.volkov.task.manager.model.User;

public interface IUserEndpoint {

    @Nullable
    User getUser(
            @Nullable Session session
    );

    @Nullable
    User editProfile(
            @Nullable Session session,
            @Nullable String firstName
    );

    @Nullable
    User editProfileWithLastName(
            @Nullable Session session,
            @Nullable String firstName,
            @Nullable String lastName
    );

    @Nullable
    User updatePassword(
            @Nullable Session session,
            @Nullable String newPassword
    );

}