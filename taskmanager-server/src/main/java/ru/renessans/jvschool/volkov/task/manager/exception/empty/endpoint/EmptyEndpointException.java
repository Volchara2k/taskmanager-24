package ru.renessans.jvschool.volkov.task.manager.exception.empty.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class EmptyEndpointException extends AbstractException {

    @NotNull
    private static final String EMPTY_ENDPOINT = "Ошибка! Параметр \"endpoint\" является null!\n";

    public EmptyEndpointException() {
        super(EMPTY_ENDPOINT);
    }

}