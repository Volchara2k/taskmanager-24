package ru.renessans.jvschool.volkov.task.manager.exception.empty.owner;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class EmptyDomainException extends AbstractException {

    @NotNull
    private static final String EMPTY_DOMAIN = "Ошибка! Параметр \"домен\" является null!\n";

    public EmptyDomainException() {
        super(EMPTY_DOMAIN);
    }

}