package ru.renessans.jvschool.volkov.task.manager.exception.empty.hash;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class EmptySignatureCycleException extends AbstractException {

    @NotNull
    private static final String EMPTY_CYCLE = "Ошибка! Параметр \"цикл для подписи\" является пустым или null!\n";

    public EmptySignatureCycleException() {
        super(EMPTY_CYCLE);
    }

}