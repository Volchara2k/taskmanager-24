package ru.renessans.jvschool.volkov.task.manager.exception.empty.owner;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class EmptyTaskException extends AbstractException {

    @NotNull
    private static final String EMPTY_TASK = "Ошибка! Параметр \"задача\" является null!\n";

    public EmptyTaskException() {
        super(EMPTY_TASK);
    }

}