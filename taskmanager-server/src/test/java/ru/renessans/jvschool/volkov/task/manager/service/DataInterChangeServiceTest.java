package ru.renessans.jvschool.volkov.task.manager.service;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IProjectUserRepository;
import ru.renessans.jvschool.volkov.task.manager.api.repository.ITaskUserRepository;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IUserRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.*;
import ru.renessans.jvschool.volkov.task.manager.casedata.CaseDomainProvider;
import ru.renessans.jvschool.volkov.task.manager.dto.Domain;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.ServiceImplementation;
import ru.renessans.jvschool.volkov.task.manager.model.Project;
import ru.renessans.jvschool.volkov.task.manager.model.Task;
import ru.renessans.jvschool.volkov.task.manager.model.User;
import ru.renessans.jvschool.volkov.task.manager.repository.ProjectUserRepository;
import ru.renessans.jvschool.volkov.task.manager.repository.TaskUserRepository;
import ru.renessans.jvschool.volkov.task.manager.repository.UserRepository;
import ru.renessans.jvschool.volkov.task.manager.util.FileUtil;

import java.io.File;
import java.util.Collection;

@RunWith(value = JUnitParamsRunner.class)
public final class DataInterChangeServiceTest {

    @NotNull
    private final IConfigurationService configService = new ConfigurationService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @NotNull
    private final ITaskUserRepository taskRepository = new TaskUserRepository();

    @NotNull
    private final ITaskUserService taskService = new TaskUserService(taskRepository);

    @NotNull
    private final IProjectUserRepository projectRepository = new ProjectUserRepository();

    @NotNull
    private final IProjectUserService projectService = new ProjectUserService(projectRepository);

    @NotNull
    private final IDomainService domainService = new DomainService(userService, taskService, projectService);

    @NotNull
    private final IDataInterChangeService dataService = new DataInterChangeService(configService, domainService);

    @Before
    public void loadConfigBefore() {
        Assert.assertNotNull(this.configService);
        this.configService.load();
    }

    @After
    public void clearTempFilesAfter() {
        FileUtil.delete(this.configService.getBase64FileName());
        FileUtil.delete(this.configService.getBinFileName());
        FileUtil.delete(this.configService.getBase64FileName());
        FileUtil.delete(this.configService.getJsonFileName());
        FileUtil.delete(this.configService.getXmlFileName());
        FileUtil.delete(this.configService.getYamlFileName());
    }

    @Test
    @TestCaseName("Run testDataBinClear for dataBinClear()")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testDataBinClear() {
        Assert.assertNotNull(this.userRepository);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.taskRepository);
        Assert.assertNotNull(this.taskService);
        Assert.assertNotNull(this.projectRepository);
        Assert.assertNotNull(this.projectService);
        Assert.assertNotNull(this.domainService);
        Assert.assertNotNull(this.configService);
        Assert.assertNotNull(this.dataService);
        @NotNull final File create = FileUtil.create(this.configService.getBinFileName());
        Assert.assertNotNull(create);

        final boolean isBinClear = this.dataService.dataBinClear();
        Assert.assertTrue(isBinClear);
    }

    @Test
    @TestCaseName("Run testDataBase64Clear for dataBase64Clear()")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testDataBase64Clear() {
        Assert.assertNotNull(this.userRepository);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.taskRepository);
        Assert.assertNotNull(this.taskService);
        Assert.assertNotNull(this.projectRepository);
        Assert.assertNotNull(this.projectService);
        Assert.assertNotNull(this.domainService);
        Assert.assertNotNull(this.configService);
        Assert.assertNotNull(this.dataService);
        @NotNull final File create = FileUtil.create(this.configService.getBase64FileName());
        Assert.assertNotNull(create);

        final boolean isBase64Clear = this.dataService.dataBase64Clear();
        Assert.assertTrue(isBase64Clear);
    }

    @Test
    @TestCaseName("Run testDataJsonClear for dataJsonClear()")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testDataJsonClear() {
        Assert.assertNotNull(this.userRepository);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.taskRepository);
        Assert.assertNotNull(this.taskService);
        Assert.assertNotNull(this.projectRepository);
        Assert.assertNotNull(this.projectService);
        Assert.assertNotNull(this.domainService);
        Assert.assertNotNull(this.configService);
        Assert.assertNotNull(this.dataService);
        @NotNull final File create = FileUtil.create(this.configService.getJsonFileName());
        Assert.assertNotNull(create);

        final boolean isJsonClear = this.dataService.dataJsonClear();
        Assert.assertTrue(isJsonClear);
    }

    @Test
    @TestCaseName("Run testDataXmlClear for dataXmlClear()")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testDataXmlClear() {
        Assert.assertNotNull(this.userRepository);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.taskRepository);
        Assert.assertNotNull(this.taskService);
        Assert.assertNotNull(this.projectRepository);
        Assert.assertNotNull(this.projectService);
        Assert.assertNotNull(this.domainService);
        Assert.assertNotNull(this.configService);
        Assert.assertNotNull(this.dataService);
        @NotNull final File create = FileUtil.create(this.configService.getXmlFileName());
        Assert.assertNotNull(create);

        final boolean isXmlClear = this.dataService.dataXmlClear();
        Assert.assertTrue(isXmlClear);
    }

    @Test
    @TestCaseName("Run testDataYamlClear for dataYamlClear()")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testDataYamlClear() {
        Assert.assertNotNull(this.userRepository);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.taskRepository);
        Assert.assertNotNull(this.taskService);
        Assert.assertNotNull(this.projectRepository);
        Assert.assertNotNull(this.projectService);
        Assert.assertNotNull(this.domainService);
        Assert.assertNotNull(this.configService);
        Assert.assertNotNull(this.dataService);
        @NotNull final File create = FileUtil.create(this.configService.getYamlFileName());
        Assert.assertNotNull(create);

        final boolean isYamlClear = this.dataService.dataYamlClear();
        Assert.assertTrue(isYamlClear);
    }

    @Test
    @TestCaseName("Run testExportDataBin for exportDataBin()")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDomainProvider.class,
            method = "validCollectionDomainsCaseData"
    )
    public void testExportDataBin(
            @NotNull final Domain data
    ) {
        Assert.assertNotNull(this.userRepository);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.taskRepository);
        Assert.assertNotNull(this.taskService);
        Assert.assertNotNull(this.projectRepository);
        Assert.assertNotNull(this.projectService);
        Assert.assertNotNull(this.domainService);
        Assert.assertNotNull(this.configService);
        Assert.assertNotNull(this.dataService);
        Assert.assertNotNull(data);
        @NotNull final Collection<User> setAllUserRecords = this.userService.setAllRecords(data.getUsers());
        Assert.assertNotNull(setAllUserRecords);
        @NotNull final Collection<Task> setAllTaskRecords = this.taskService.setAllRecords(data.getTasks());
        Assert.assertNotNull(setAllTaskRecords);
        @NotNull final Collection<Project> setAllProjectRecords = this.projectService.setAllRecords(data.getProjects());
        Assert.assertNotNull(setAllProjectRecords);

        @NotNull final Domain domain = this.dataService.exportDataBin();
        Assert.assertNotNull(domain);
        Assert.assertEquals(setAllUserRecords, domain.getUsers());
        Assert.assertEquals(setAllTaskRecords, domain.getTasks());
        Assert.assertEquals(setAllProjectRecords, domain.getProjects());
    }

    @Test
    @TestCaseName("Run testExportDataBase64 for exportDataBase64()")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDomainProvider.class,
            method = "validCollectionDomainsCaseData"
    )
    public void testExportDataBase64(
            @NotNull final Domain data
    ) {
        Assert.assertNotNull(this.userRepository);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.taskRepository);
        Assert.assertNotNull(this.taskService);
        Assert.assertNotNull(this.projectRepository);
        Assert.assertNotNull(this.projectService);
        Assert.assertNotNull(this.domainService);
        Assert.assertNotNull(this.configService);
        Assert.assertNotNull(this.dataService);
        Assert.assertNotNull(data);
        @NotNull final Collection<User> setAllUserRecords = this.userService.setAllRecords(data.getUsers());
        Assert.assertNotNull(setAllUserRecords);
        @NotNull final Collection<Task> setAllTaskRecords = this.taskService.setAllRecords(data.getTasks());
        Assert.assertNotNull(setAllTaskRecords);
        @NotNull final Collection<Project> setAllProjectRecords = this.projectService.setAllRecords(data.getProjects());
        Assert.assertNotNull(setAllProjectRecords);

        @NotNull final Domain domain = this.dataService.exportDataBase64();
        Assert.assertNotNull(domain);
        Assert.assertEquals(setAllUserRecords, domain.getUsers());
        Assert.assertEquals(setAllTaskRecords, domain.getTasks());
        Assert.assertEquals(setAllProjectRecords, domain.getProjects());
    }

    @Test
    @TestCaseName("Run testExportDataJson for exportDataJson()")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDomainProvider.class,
            method = "validCollectionDomainsCaseData"
    )
    public void testExportDataJson(
            @NotNull final Domain data
    ) {
        Assert.assertNotNull(this.userRepository);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.taskRepository);
        Assert.assertNotNull(this.taskService);
        Assert.assertNotNull(this.projectRepository);
        Assert.assertNotNull(this.projectService);
        Assert.assertNotNull(this.domainService);
        Assert.assertNotNull(this.configService);
        Assert.assertNotNull(this.dataService);
        Assert.assertNotNull(data);
        @NotNull final Collection<User> setAllUserRecords = this.userService.setAllRecords(data.getUsers());
        Assert.assertNotNull(setAllUserRecords);
        @NotNull final Collection<Task> setAllTaskRecords = this.taskService.setAllRecords(data.getTasks());
        Assert.assertNotNull(setAllTaskRecords);
        @NotNull final Collection<Project> setAllProjectRecords = this.projectService.setAllRecords(data.getProjects());
        Assert.assertNotNull(setAllProjectRecords);

        @NotNull final Domain domain = this.dataService.exportDataJson();
        Assert.assertNotNull(domain);
        Assert.assertEquals(setAllUserRecords, domain.getUsers());
        Assert.assertEquals(setAllTaskRecords, domain.getTasks());
        Assert.assertEquals(setAllProjectRecords, domain.getProjects());
    }

    @Test
    @TestCaseName("Run testExportDataXml for exportDataXml()")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDomainProvider.class,
            method = "validCollectionDomainsCaseData"
    )
    public void testExportDataXml(
            @NotNull final Domain data
    ) {
        Assert.assertNotNull(this.userRepository);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.taskRepository);
        Assert.assertNotNull(this.taskService);
        Assert.assertNotNull(this.projectRepository);
        Assert.assertNotNull(this.projectService);
        Assert.assertNotNull(this.domainService);
        Assert.assertNotNull(this.configService);
        Assert.assertNotNull(this.dataService);
        Assert.assertNotNull(data);
        @NotNull final Collection<User> setAllUserRecords = this.userService.setAllRecords(data.getUsers());
        Assert.assertNotNull(setAllUserRecords);
        @NotNull final Collection<Task> setAllTaskRecords = this.taskService.setAllRecords(data.getTasks());
        Assert.assertNotNull(setAllTaskRecords);
        @NotNull final Collection<Project> setAllProjectRecords = this.projectService.setAllRecords(data.getProjects());
        Assert.assertNotNull(setAllProjectRecords);

        @NotNull final Domain domain = this.dataService.exportDataXml();
        Assert.assertNotNull(domain);
        Assert.assertEquals(setAllUserRecords, domain.getUsers());
        Assert.assertEquals(setAllTaskRecords, domain.getTasks());
        Assert.assertEquals(setAllProjectRecords, domain.getProjects());
    }

    @Test
    @TestCaseName("Run testExportDataYaml for exportDataYaml()")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDomainProvider.class,
            method = "validCollectionDomainsCaseData"
    )
    public void testExportDataYaml(
            @NotNull final Domain data
    ) {
        Assert.assertNotNull(this.userRepository);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.taskRepository);
        Assert.assertNotNull(this.taskService);
        Assert.assertNotNull(this.projectRepository);
        Assert.assertNotNull(this.projectService);
        Assert.assertNotNull(this.domainService);
        Assert.assertNotNull(this.configService);
        Assert.assertNotNull(this.dataService);
        Assert.assertNotNull(data);
        @NotNull final Collection<User> setAllUserRecords = this.userService.setAllRecords(data.getUsers());
        Assert.assertNotNull(setAllUserRecords);
        @NotNull final Collection<Task> setAllTaskRecords = this.taskService.setAllRecords(data.getTasks());
        Assert.assertNotNull(setAllTaskRecords);
        @NotNull final Collection<Project> setAllProjectRecords = this.projectService.setAllRecords(data.getProjects());
        Assert.assertNotNull(setAllProjectRecords);

        @NotNull final Domain domain = this.dataService.exportDataYaml();
        Assert.assertNotNull(domain);
        Assert.assertEquals(setAllUserRecords, domain.getUsers());
        Assert.assertEquals(setAllTaskRecords, domain.getTasks());
        Assert.assertEquals(setAllProjectRecords, domain.getProjects());
    }

    @Test
    @TestCaseName("Run testImportDataBin for importDataBin()")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDomainProvider.class,
            method = "validCollectionDomainsCaseData"
    )
    public void testImportDataBin(
            @NotNull final Domain data
    ) {
        Assert.assertNotNull(this.userRepository);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.taskRepository);
        Assert.assertNotNull(this.taskService);
        Assert.assertNotNull(this.projectRepository);
        Assert.assertNotNull(this.projectService);
        Assert.assertNotNull(this.domainService);
        Assert.assertNotNull(this.configService);
        Assert.assertNotNull(this.dataService);
        Assert.assertNotNull(data);
        @NotNull final Collection<User> setAllUserRecords = this.userService.setAllRecords(data.getUsers());
        Assert.assertNotNull(setAllUserRecords);
        @NotNull final Collection<Task> setAllTaskRecords = this.taskService.setAllRecords(data.getTasks());
        Assert.assertNotNull(setAllTaskRecords);
        @NotNull final Collection<Project> setAllProjectRecords = this.projectService.setAllRecords(data.getProjects());
        Assert.assertNotNull(setAllProjectRecords);
        @NotNull final Domain exportDomain = this.dataService.exportDataBin();
        Assert.assertNotNull(exportDomain);

        @NotNull final Domain domain = this.dataService.importDataBin();
        Assert.assertNotNull(domain);
        @NotNull final Collection<User> importDomainUsers = domain.getUsers();
        Assert.assertNotNull(importDomainUsers);
        Assert.assertNotEquals(0, importDomainUsers.size());
        @NotNull final Collection<Task> importDomainTasks = domain.getTasks();
        Assert.assertNotNull(importDomainTasks);
        Assert.assertNotEquals(0, importDomainTasks.size());
        @NotNull final Collection<Project> importDomainProjects = domain.getProjects();
        Assert.assertNotNull(importDomainProjects);
        Assert.assertNotEquals(0, importDomainProjects.size());
        Assert.assertEquals(importDomainUsers, this.userService.getAllRecords());
        Assert.assertEquals(importDomainTasks, this.taskService.getAllRecords());
        Assert.assertEquals(importDomainProjects, this.projectService.getAllRecords());
    }

    @Test
    @TestCaseName("Run testImportDataBase64 for importDataBase64()")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDomainProvider.class,
            method = "validCollectionDomainsCaseData"
    )
    public void testImportDataBase64(
            @NotNull final Domain data
    ) {
        Assert.assertNotNull(this.userRepository);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.taskRepository);
        Assert.assertNotNull(this.taskService);
        Assert.assertNotNull(this.projectRepository);
        Assert.assertNotNull(this.projectService);
        Assert.assertNotNull(this.domainService);
        Assert.assertNotNull(this.configService);
        Assert.assertNotNull(this.dataService);
        Assert.assertNotNull(data);
        @NotNull final Collection<User> setAllUserRecords = this.userService.setAllRecords(data.getUsers());
        Assert.assertNotNull(setAllUserRecords);
        @NotNull final Collection<Task> setAllTaskRecords = this.taskService.setAllRecords(data.getTasks());
        Assert.assertNotNull(setAllTaskRecords);
        @NotNull final Collection<Project> setAllProjectRecords = this.projectService.setAllRecords(data.getProjects());
        Assert.assertNotNull(setAllProjectRecords);
        @NotNull final Domain exportDomain = this.dataService.exportDataBase64();
        Assert.assertNotNull(exportDomain);

        @NotNull final Domain domain = this.dataService.importDataBase64();
        Assert.assertNotNull(domain);
        @NotNull final Collection<User> importDomainUsers = domain.getUsers();
        Assert.assertNotNull(importDomainUsers);
        Assert.assertNotEquals(0, importDomainUsers.size());
        @NotNull final Collection<Task> importDomainTasks = domain.getTasks();
        Assert.assertNotNull(importDomainTasks);
        Assert.assertNotEquals(0, importDomainTasks.size());
        @NotNull final Collection<Project> importDomainProjects = domain.getProjects();
        Assert.assertNotNull(importDomainProjects);
        Assert.assertNotEquals(0, importDomainProjects.size());
        Assert.assertEquals(importDomainUsers, this.userService.getAllRecords());
        Assert.assertEquals(importDomainTasks, this.taskService.getAllRecords());
        Assert.assertEquals(importDomainProjects, this.projectService.getAllRecords());
    }

    @Test
    @TestCaseName("Run testImportDataJson for importDataJson()")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDomainProvider.class,
            method = "validCollectionDomainsCaseData"
    )
    public void testImportDataJson(
            @NotNull final Domain data
    ) {
        Assert.assertNotNull(this.userRepository);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.taskRepository);
        Assert.assertNotNull(this.taskService);
        Assert.assertNotNull(this.projectRepository);
        Assert.assertNotNull(this.projectService);
        Assert.assertNotNull(this.domainService);
        Assert.assertNotNull(this.configService);
        Assert.assertNotNull(this.dataService);
        Assert.assertNotNull(data);
        @NotNull final Collection<User> setAllUserRecords = this.userService.setAllRecords(data.getUsers());
        Assert.assertNotNull(setAllUserRecords);
        @NotNull final Collection<Task> setAllTaskRecords = this.taskService.setAllRecords(data.getTasks());
        Assert.assertNotNull(setAllTaskRecords);
        @NotNull final Collection<Project> setAllProjectRecords = this.projectService.setAllRecords(data.getProjects());
        Assert.assertNotNull(setAllProjectRecords);
        @NotNull final Domain exportDomain = this.dataService.exportDataJson();
        Assert.assertNotNull(exportDomain);

        @NotNull final Domain domain = this.dataService.importDataJson();
        Assert.assertNotNull(domain);
        @NotNull final Collection<User> importDomainUsers = domain.getUsers();
        Assert.assertNotNull(importDomainUsers);
        @NotNull final Collection<Task> importDomainTasks = domain.getTasks();
        Assert.assertNotNull(importDomainTasks);
        @NotNull final Collection<Project> importDomainProjects = domain.getProjects();
        Assert.assertNotNull(importDomainProjects);
        Assert.assertEquals(importDomainUsers, this.userService.getAllRecords());
        Assert.assertEquals(importDomainTasks, this.taskService.getAllRecords());
        Assert.assertEquals(importDomainProjects, this.projectService.getAllRecords());
    }

    @Test
    @TestCaseName("Run testImportDataXml for importDataXml()")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDomainProvider.class,
            method = "validCollectionDomainsCaseData"
    )
    public void testImportDataXml(
            @NotNull final Domain data
    ) {
        Assert.assertNotNull(this.userRepository);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.taskRepository);
        Assert.assertNotNull(this.taskService);
        Assert.assertNotNull(this.projectRepository);
        Assert.assertNotNull(this.projectService);
        Assert.assertNotNull(this.domainService);
        Assert.assertNotNull(this.configService);
        Assert.assertNotNull(this.dataService);
        Assert.assertNotNull(data);
        @NotNull final Collection<User> setAllUserRecords = this.userService.setAllRecords(data.getUsers());
        Assert.assertNotNull(setAllUserRecords);
        @NotNull final Collection<Task> setAllTaskRecords = this.taskService.setAllRecords(data.getTasks());
        Assert.assertNotNull(setAllTaskRecords);
        @NotNull final Collection<Project> setAllProjectRecords = this.projectService.setAllRecords(data.getProjects());
        Assert.assertNotNull(setAllProjectRecords);
        @NotNull final Domain exportDomain = this.dataService.exportDataXml();
        Assert.assertNotNull(exportDomain);

        @NotNull final Domain domain = this.dataService.importDataXml();
        Assert.assertNotNull(domain);
        @NotNull final Collection<User> importDomainUsers = domain.getUsers();
        Assert.assertNotNull(importDomainUsers);
        @NotNull final Collection<Task> importDomainTasks = domain.getTasks();
        Assert.assertNotNull(importDomainTasks);
        @NotNull final Collection<Project> importDomainProjects = domain.getProjects();
        Assert.assertNotNull(importDomainProjects);
        Assert.assertEquals(importDomainUsers, this.userService.getAllRecords());
        Assert.assertEquals(importDomainTasks, this.taskService.getAllRecords());
        Assert.assertEquals(importDomainProjects, this.projectService.getAllRecords());
    }

    @Test
    @TestCaseName("Run testImportDataYaml for importDataYaml()")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDomainProvider.class,
            method = "validCollectionDomainsCaseData"
    )
    public void testImportDataYaml(
            @NotNull final Domain data
    ) {
        Assert.assertNotNull(this.userRepository);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.taskRepository);
        Assert.assertNotNull(this.taskService);
        Assert.assertNotNull(this.projectRepository);
        Assert.assertNotNull(this.projectService);
        Assert.assertNotNull(this.domainService);
        Assert.assertNotNull(this.configService);
        Assert.assertNotNull(this.dataService);
        Assert.assertNotNull(data);
        @NotNull final Collection<User> setAllUserRecords = this.userService.setAllRecords(data.getUsers());
        Assert.assertNotNull(setAllUserRecords);
        @NotNull final Collection<Task> setAllTaskRecords = this.taskService.setAllRecords(data.getTasks());
        Assert.assertNotNull(setAllTaskRecords);
        @NotNull final Collection<Project> setAllProjectRecords = this.projectService.setAllRecords(data.getProjects());
        Assert.assertNotNull(setAllProjectRecords);
        @NotNull final Domain exportDomain = this.dataService.exportDataYaml();
        Assert.assertNotNull(exportDomain);

        @NotNull final Domain domain = this.dataService.importDataYaml();
        Assert.assertNotNull(domain);
        @NotNull final Collection<User> importDomainUsers = domain.getUsers();
        Assert.assertNotNull(importDomainUsers);
        @NotNull final Collection<Task> importDomainTasks = domain.getTasks();
        Assert.assertNotNull(importDomainTasks);
        @NotNull final Collection<Project> importDomainProjects = domain.getProjects();
        Assert.assertNotNull(importDomainProjects);
        Assert.assertEquals(importDomainUsers, this.userService.getAllRecords());
        Assert.assertEquals(importDomainTasks, this.taskService.getAllRecords());
        Assert.assertEquals(importDomainProjects, this.projectService.getAllRecords());
    }

}