package ru.renessans.jvschool.volkov.task.manager.endpoint;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.api.IServiceLocatorRepository;
import ru.renessans.jvschool.volkov.task.manager.api.IServiceLocatorService;
import ru.renessans.jvschool.volkov.task.manager.api.endpoint.IAdminEndpoint;
import ru.renessans.jvschool.volkov.task.manager.api.service.*;
import ru.renessans.jvschool.volkov.task.manager.casedata.CaseDataProjectProvider;
import ru.renessans.jvschool.volkov.task.manager.casedata.CaseDataTaskProvider;
import ru.renessans.jvschool.volkov.task.manager.casedata.CaseDataUserProvider;
import ru.renessans.jvschool.volkov.task.manager.constant.DemoDataConst;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRole;
import ru.renessans.jvschool.volkov.task.manager.marker.EndpointImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.model.Project;
import ru.renessans.jvschool.volkov.task.manager.model.Session;
import ru.renessans.jvschool.volkov.task.manager.model.Task;
import ru.renessans.jvschool.volkov.task.manager.model.User;
import ru.renessans.jvschool.volkov.task.manager.repository.ServiceLocatorRepository;
import ru.renessans.jvschool.volkov.task.manager.service.ServiceLocatorService;
import ru.renessans.jvschool.volkov.task.manager.util.HashUtil;

import java.util.Collection;

@RunWith(value = JUnitParamsRunner.class)
@Category({PositiveImplementation.class, EndpointImplementation.class})
public final class AdminEndpointTest {

    @NotNull
    private final IServiceLocatorRepository serviceLocatorRepository = new ServiceLocatorRepository();

    @NotNull
    private final IServiceLocatorService serviceLocator = new ServiceLocatorService(serviceLocatorRepository);

    @NotNull
    private final ISessionService sessionService = serviceLocator.getSessionService();

    @NotNull
    private final IAuthenticationService authService = serviceLocator.getAuthenticationService();

    @NotNull
    private final IUserService userService = serviceLocator.getUserService();

    @NotNull
    private final IConfigurationService configService = serviceLocator.getConfigurationService();

    @NotNull
    private final ITaskUserService taskService = serviceLocator.getTaskService();

    @NotNull
    private final IProjectUserService projectService = serviceLocator.getProjectService();

    @NotNull
    private final IAdminEndpoint adminEndpoint = new AdminEndpoint(serviceLocator);

    @Before
    public void initialAdminRecordBefore() {
        @NotNull final User addAdminRecord = this.userService.addUser(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD, UserRole.ADMIN
        );
        Assert.assertNotNull(addAdminRecord);
        this.configService.load();
    }

    @Test
    @TestCaseName("Run testCloseAllSessions for closeAllSessions(session)")
    public void testCloseAllSessions() {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocator);
        Assert.assertNotNull(this.adminEndpoint);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.authService);
        @NotNull final Session open = this.sessionService.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        final boolean closeSessions = this.adminEndpoint.closeAllSessions(open);
        Assert.assertTrue(closeSessions);
    }

    @Test
    @TestCaseName("Run testGetAllSessions for getAllSessions(session)")
    public void testGetAllSessions() {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocator);
        Assert.assertNotNull(this.adminEndpoint);
        @NotNull final Session open = this.sessionService.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @NotNull final Collection<Session> allSessions = this.adminEndpoint.getAllSessions(open);
        Assert.assertNotNull(allSessions);
        Assert.assertNotEquals(0, allSessions.size());
    }

    @Test
    @TestCaseName("Run testSignUpUserWithUserRole for signUpUserWithUserRole(session, \"{0}\", \"{1}\", {2})")
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "validUsersMainFieldsWithRoleCaseData"
    )
    public void testSignUpUserWithUserRole(
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final UserRole userRole
    ) {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocator);
        Assert.assertNotNull(this.adminEndpoint);
        Assert.assertNotNull(login);
        Assert.assertNotNull(password);
        Assert.assertNotNull(userRole);
        @NotNull final Session open = this.sessionService.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @NotNull final User addUser = this.adminEndpoint.signUpUserWithUserRole(open, login, password, userRole);
        Assert.assertNotNull(addUser);
        Assert.assertEquals(login, addUser.getLogin());
        @NotNull final String hashPassword = HashUtil.getSaltHashLine(password);
        Assert.assertEquals(hashPassword, addUser.getPasswordHash());
        Assert.assertEquals(userRole, addUser.getRole());
    }

    @Test
    @TestCaseName("Run testDeleteUserById for deleteUserById(session, \"{0}\", \"{1}\")")
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "validUsersMainFieldsCaseData"
    )
    public void testDeleteUserById(
            @NotNull final String login,
            @NotNull final String password
    ) {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocator);
        Assert.assertNotNull(this.adminEndpoint);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(login);
        Assert.assertNotNull(password);
        @NotNull final User addRecord = this.userService.addUser(login, password);
        Assert.assertNotNull(addRecord);
        @NotNull final Session open = this.sessionService.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @Nullable final User deleteUser = this.adminEndpoint.deleteUserById(open, addRecord.getId());
        Assert.assertNotNull(deleteUser);
        Assert.assertEquals(addRecord.getId(), deleteUser.getId());
        Assert.assertEquals(login, deleteUser.getLogin());
        @NotNull final String hashPassword = HashUtil.getSaltHashLine(password);
        Assert.assertEquals(hashPassword, deleteUser.getPasswordHash());
        Assert.assertEquals(addRecord.getRole(), deleteUser.getRole());
    }

    @Test
    @TestCaseName("Run testDeleteUserByLogin for deleteUserByLogin(session, \"{0}\", \"{1}\")")
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "validUsersMainFieldsCaseData"
    )
    public void testDeleteUserByLogin(
            @NotNull final String login,
            @NotNull final String password
    ) {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocator);
        Assert.assertNotNull(this.adminEndpoint);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(login);
        Assert.assertNotNull(password);
        @NotNull final User addRecord = this.userService.addUser(login, password);
        Assert.assertNotNull(addRecord);
        @NotNull final Session open = this.sessionService.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @Nullable final User deleteUser = this.adminEndpoint.deleteUserByLogin(open, login);
        Assert.assertNotNull(deleteUser);
        Assert.assertEquals(addRecord.getId(), deleteUser.getId());
        Assert.assertEquals(login, deleteUser.getLogin());
        @NotNull final String hashPassword = HashUtil.getSaltHashLine(password);
        Assert.assertEquals(hashPassword, deleteUser.getPasswordHash());
        Assert.assertEquals(addRecord.getRole(), deleteUser.getRole());
    }

    @Test
    @TestCaseName("Run testDeleteAllUsers for deleteAllUsers(session)")
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "validUsersMainFieldsCaseData"
    )
    public void testDeleteAllUsers(
            @NotNull final String login,
            @NotNull final String password
    ) {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocator);
        Assert.assertNotNull(this.adminEndpoint);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(login);
        Assert.assertNotNull(password);
        @NotNull final User addRecord = this.userService.addUser(login, password);
        Assert.assertNotNull(addRecord);
        @NotNull final Session open = this.sessionService.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @NotNull final Collection<User> deleteAll = this.adminEndpoint.deleteAllUsers(open);
        Assert.assertNotNull(deleteAll);
        Assert.assertNotEquals(0, deleteAll.size());
    }

    @Test
    @TestCaseName("Run testLockUserByLogin for lockUserByLogin(session, \"{0}\", \"{1}\")")
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "validUsersMainFieldsCaseData"
    )
    public void testLockUserByLogin(
            @NotNull final String login,
            @NotNull final String password
    ) {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocator);
        Assert.assertNotNull(this.adminEndpoint);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(login);
        Assert.assertNotNull(password);
        @NotNull final User addRecord = this.userService.addUser(login, password);
        Assert.assertNotNull(addRecord);
        @NotNull final Session open = this.sessionService.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @Nullable final User lockUser = this.adminEndpoint.lockUserByLogin(open, login);
        Assert.assertNotNull(lockUser);
        Assert.assertEquals(addRecord.getId(), lockUser.getId());
        Assert.assertEquals(login, lockUser.getLogin());
        @NotNull final String hashPassword = HashUtil.getSaltHashLine(password);
        Assert.assertEquals(hashPassword, lockUser.getPasswordHash());
        Assert.assertEquals(addRecord.getRole(), lockUser.getRole());
        Assert.assertTrue(lockUser.getLockdown());
    }

    @Test
    @TestCaseName("Run testUnlockUserByLogin for unlockUserByLogin(session, \"{0}\", \"{1}\")")
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "validUsersMainFieldsCaseData"
    )
    public void testUnlockUserByLogin(
            @NotNull final String login,
            @NotNull final String password
    ) {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocator);
        Assert.assertNotNull(this.adminEndpoint);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(login);
        Assert.assertNotNull(password);
        @NotNull final User addRecord = this.userService.addUser(login, password);
        Assert.assertNotNull(addRecord);
        @Nullable final User lockUser = this.userService.lockUserByLogin(login);
        Assert.assertNotNull(lockUser);
        @NotNull final Session open = this.sessionService.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @Nullable final User unlockUser = this.adminEndpoint.unlockUserByLogin(open, login);
        Assert.assertNotNull(unlockUser);
        Assert.assertEquals(addRecord.getId(), unlockUser.getId());
        Assert.assertEquals(login, unlockUser.getLogin());
        @NotNull final String hashPassword = HashUtil.getSaltHashLine(password);
        Assert.assertEquals(hashPassword, unlockUser.getPasswordHash());
        Assert.assertEquals(addRecord.getRole(), unlockUser.getRole());
        Assert.assertFalse(unlockUser.getLockdown());
    }

    @Test
    @TestCaseName("Run testGetAllUsers for getAllUsers(session)")
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "validUsersMainFieldsCaseData"
    )
    public void testGetAllUsers(
            @NotNull final String login,
            @NotNull final String password
    ) {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocator);
        Assert.assertNotNull(this.adminEndpoint);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(login);
        Assert.assertNotNull(password);
        @NotNull final User addRecord = this.userService.addUser(login, password);
        Assert.assertNotNull(addRecord);
        @NotNull final Session open = this.sessionService.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @Nullable final Collection<User> getUser = this.adminEndpoint.getAllUsers(open);
        Assert.assertNotNull(getUser);
        Assert.assertNotEquals(0, getUser.size());
    }

    @Test
    @TestCaseName("Run testGetAllUsersTasks for getAllUsersTasks(session)")
    @Parameters(
            source = CaseDataTaskProvider.class,
            method = "validTasksCaseData"
    )
    public void testGetAllUsersTasks(
            @NotNull final User user,
            @NotNull final Task task
    ) {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocator);
        Assert.assertNotNull(this.adminEndpoint);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.taskService);
        Assert.assertNotNull(user);
        Assert.assertNotNull(task);
        @NotNull final Task addRecord = this.taskService.addRecord(task);
        Assert.assertNotNull(addRecord);
        @NotNull final Session open = this.sessionService.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @NotNull final Collection<Task> allUsersTasks = this.adminEndpoint.getAllUsersTasks(open);
        Assert.assertNotNull(allUsersTasks);
        Assert.assertNotEquals(0, allUsersTasks.size());
    }

    @Test
    @TestCaseName("Run testGetAllUsersProjects for getAllUsersProjects(session)")
    @Parameters(
            source = CaseDataProjectProvider.class,
            method = "validProjectsCaseData"
    )
    public void testGetAllUsersProjects(
            @NotNull final User user,
            @NotNull final Project project
    ) {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocator);
        Assert.assertNotNull(this.adminEndpoint);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.taskService);
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        @NotNull final Project addRecord = this.projectService.addRecord(project);
        Assert.assertNotNull(addRecord);
        @NotNull final Session open = this.sessionService.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @NotNull final Collection<Project> allUsersProjects = this.adminEndpoint.getAllUsersProjects(open);
        Assert.assertNotNull(allUsersProjects);
        Assert.assertNotEquals(0, allUsersProjects.size());
    }

    @Test
    @TestCaseName("Run testGetUserById for getUserById(session, \"{0}\", \"{1}\")")
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "validUsersMainFieldsCaseData"
    )
    public void testGetUserById(
            @NotNull final String login,
            @NotNull final String password
    ) {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocator);
        Assert.assertNotNull(this.adminEndpoint);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(login);
        Assert.assertNotNull(password);
        @NotNull final User addRecord = this.userService.addUser(login, password);
        Assert.assertNotNull(addRecord);
        @NotNull final Session open = this.sessionService.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @Nullable final User getUser = this.adminEndpoint.getUserById(open, addRecord.getId());
        Assert.assertNotNull(getUser);
        Assert.assertEquals(addRecord.getId(), getUser.getId());
        Assert.assertEquals(login, getUser.getLogin());
        @NotNull final String hashPassword = HashUtil.getSaltHashLine(password);
        Assert.assertEquals(addRecord.getRole(), getUser.getRole());
        Assert.assertEquals(hashPassword, getUser.getPasswordHash());
    }

    @Test
    @TestCaseName("Run testGetUserByLogin for getUserByLogin(session, \"{0}\", \"{1}\")")
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "validUsersMainFieldsCaseData"
    )
    public void testGetUserByLogin(
            @NotNull final String login,
            @NotNull final String password
    ) {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocator);
        Assert.assertNotNull(this.adminEndpoint);
        Assert.assertNotNull(this.userService);
        @NotNull final User addRecord = this.userService.addUser(login, password);
        Assert.assertNotNull(addRecord);
        @NotNull final Session open = this.sessionService.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @Nullable final User getUser = this.adminEndpoint.getUserByLogin(open, addRecord.getLogin());
        Assert.assertNotNull(getUser);
        Assert.assertEquals(addRecord.getId(), getUser.getId());
        Assert.assertEquals(login, getUser.getLogin());
        @NotNull final String hashPassword = HashUtil.getSaltHashLine(password);
        Assert.assertEquals(addRecord.getRole(), getUser.getRole());
        Assert.assertEquals(hashPassword, getUser.getPasswordHash());
    }

    @Test
    @TestCaseName("Run testEditProfileById for editProfileById(session, \"{0}\", \"{1}\", \"{2}\")")
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "validUsersMainFieldsWithNewEntityCaseData"
    )
    public void testEditProfileById(
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final String newFirstName
    ) {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocator);
        Assert.assertNotNull(this.adminEndpoint);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(login);
        Assert.assertNotNull(password);
        Assert.assertNotNull(newFirstName);
        @NotNull final User addRecord = this.userService.addUser(login, password);
        Assert.assertNotNull(addRecord);
        @NotNull final Session open = this.sessionService.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @Nullable final User editUser = this.adminEndpoint.editProfileById(open, addRecord.getId(), newFirstName);
        Assert.assertNotNull(editUser);
        Assert.assertEquals(addRecord.getId(), editUser.getId());
        Assert.assertEquals(login, editUser.getLogin());
        @NotNull final String hashPassword = HashUtil.getSaltHashLine(password);
        Assert.assertEquals(hashPassword, editUser.getPasswordHash());
        Assert.assertEquals(addRecord.getRole(), editUser.getRole());
        Assert.assertEquals(newFirstName, editUser.getFirstName());
    }

    @Test
    @TestCaseName("Run testUpdatePasswordById for updatePasswordById(session, \"{0}\", \"{1}\", \"{2}\")")
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "validUsersMainFieldsWithNewEntityCaseData"
    )
    public void testUpdatePasswordById(
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final String newPassword
    ) {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocator);
        Assert.assertNotNull(this.adminEndpoint);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(login);
        Assert.assertNotNull(password);
        Assert.assertNotNull(newPassword);
        @NotNull final User addRecord = this.userService.addUser(login, password);
        Assert.assertNotNull(addRecord);
        @NotNull final Session open = this.sessionService.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @Nullable final User updatePassword = this.adminEndpoint.updatePasswordById(open, addRecord.getId(), newPassword);
        Assert.assertNotNull(updatePassword);
        Assert.assertEquals(addRecord.getId(), updatePassword.getId());
        Assert.assertEquals(login, updatePassword.getLogin());
        @NotNull final String hashPassword = HashUtil.getSaltHashLine(newPassword);
        Assert.assertEquals(addRecord.getRole(), updatePassword.getRole());
        Assert.assertEquals(hashPassword, updatePassword.getPasswordHash());
    }

}