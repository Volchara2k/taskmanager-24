package ru.renessans.jvschool.volkov.task.manager.repository;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.api.repository.ISessionRepository;
import ru.renessans.jvschool.volkov.task.manager.casedata.CaseDataSessionProvider;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.RepositoryImplementation;
import ru.renessans.jvschool.volkov.task.manager.model.Session;
import ru.renessans.jvschool.volkov.task.manager.model.User;

@RunWith(value = JUnitParamsRunner.class)
public final class SessionRepositoryTest {

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @Test
    @TestCaseName("Run testContainsUserId for containsUserId({0})")
    @Category({PositiveImplementation.class, RepositoryImplementation.class})
    @Parameters(
            source = CaseDataSessionProvider.class,
            method = "validSessionsCaseData"
    )
    public void testContainsUserId(
            @NotNull final User user,
            @NotNull final Session session
    ) {
        Assert.assertNotNull(user);
        Assert.assertNotNull(session);
        Assert.assertNotNull(this.sessionRepository);
        @NotNull final Session addRecord = this.sessionRepository.addRecord(session);
        Assert.assertNotNull(addRecord);

        final boolean isContainsUser = this.sessionRepository.containsUserId(user.getId());
        Assert.assertTrue(isContainsUser);
    }

    @Test
    @TestCaseName("Run testGetSessionByUserId for getSessionByUserId({0})")
    @Category({PositiveImplementation.class, RepositoryImplementation.class})
    @Parameters(
            source = CaseDataSessionProvider.class,
            method = "validSessionsCaseData"
    )
    public void testGetSessionByUserId(
            @NotNull final User user,
            @NotNull final Session session
    ) {
        Assert.assertNotNull(user);
        Assert.assertNotNull(session);
        Assert.assertNotNull(this.sessionRepository);
        @NotNull final Session addRecord = this.sessionRepository.addRecord(session);
        Assert.assertNotNull(addRecord);

        @Nullable final Session getSession = this.sessionRepository.getSessionByUserId(user.getId());
        Assert.assertNotNull(getSession);
        Assert.assertEquals(session.getId(), getSession.getId());
        Assert.assertEquals(session.getUserId(), getSession.getUserId());
        Assert.assertEquals(session.getTimestamp(), getSession.getTimestamp());
    }

    @Test
    @TestCaseName("Run testDeleteByUserId for deleteByUserId({0})")
    @Category({PositiveImplementation.class, RepositoryImplementation.class})
    @Parameters(
            source = CaseDataSessionProvider.class,
            method = "validSessionsCaseData"
    )
    public void testDeleteByUserId(
            @NotNull final User user,
            @NotNull final Session session
    ) {
        Assert.assertNotNull(user);
        Assert.assertNotNull(session);
        Assert.assertNotNull(this.sessionRepository);
        @NotNull final Session addRecord = this.sessionRepository.addRecord(session);
        Assert.assertNotNull(addRecord);

        final boolean isDeleteSession = this.sessionRepository.deleteByUserId(user.getId());
        Assert.assertTrue(isDeleteSession);
    }

}