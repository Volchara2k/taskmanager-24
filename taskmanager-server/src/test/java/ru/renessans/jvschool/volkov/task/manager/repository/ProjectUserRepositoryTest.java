package ru.renessans.jvschool.volkov.task.manager.repository;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IProjectUserRepository;
import ru.renessans.jvschool.volkov.task.manager.casedata.CaseDataProjectProvider;
import ru.renessans.jvschool.volkov.task.manager.exception.empty.owner.EmptyProjectException;
import ru.renessans.jvschool.volkov.task.manager.marker.NegativeImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.RepositoryImplementation;
import ru.renessans.jvschool.volkov.task.manager.model.Project;
import ru.renessans.jvschool.volkov.task.manager.model.User;

import java.util.Collection;

@RunWith(value = JUnitParamsRunner.class)
public final class ProjectUserRepositoryTest {

    @NotNull
    private final IProjectUserRepository projectRepository = new ProjectUserRepository();

    @Test(expected = EmptyProjectException.class)
    @TestCaseName("Run testNegativeDeleteBy for deleteById({0}, incorrect)")
    @Category({NegativeImplementation.class, RepositoryImplementation.class})
    @Parameters(
            source = CaseDataProjectProvider.class,
            method = "validProjectsCaseData"
    )
    public void testNegativeDeleteById(
            @NotNull final User user,
            @NotNull final Project project
    ) {
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        Assert.assertNotNull(this.projectRepository);
        @NotNull final Project addRecord = this.projectRepository.addRecord(project);
        Assert.assertNotNull(addRecord);
        this.projectRepository.deleteById(user.getId(), project.getId() + ".");
    }

    @Test(expected = EmptyProjectException.class)
    @TestCaseName("Run testNegativeDeleteByTitle for deleteByTitle({0}, incorrect)")
    @Category({NegativeImplementation.class, RepositoryImplementation.class})
    @Parameters(
            source = CaseDataProjectProvider.class,
            method = "validProjectsCaseData"
    )
    public void testNegativeDeleteByTitle(
            @NotNull final User user,
            @NotNull final Project project
    ) {
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        Assert.assertNotNull(this.projectRepository);
        @NotNull final Project addRecord = this.projectRepository.addRecord(project);
        Assert.assertNotNull(addRecord);
        this.projectRepository.deleteByTitle(user.getId(), project.getTitle() + ".");
    }

    @Test
    @TestCaseName("Run testDeleteByIndex for deleteByIndex({0}, 0)")
    @Category({PositiveImplementation.class, RepositoryImplementation.class})
    @Parameters(
            source = CaseDataProjectProvider.class,
            method = "validProjectsCaseData"
    )
    public void testDeleteByIndex(
            @NotNull final User user,
            @NotNull final Project project
    ) {
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        Assert.assertNotNull(this.projectRepository);
        @NotNull final Project addRecord = this.projectRepository.addRecord(project);
        Assert.assertNotNull(addRecord);

        @Nullable final Project deleteProject = this.projectRepository.deleteByIndex(user.getId(), 0);
        Assert.assertNotNull(deleteProject);
        Assert.assertEquals(project.getId(), deleteProject.getId());
        Assert.assertEquals(project.getUserId(), deleteProject.getUserId());
        Assert.assertEquals(project.getTitle(), deleteProject.getTitle());
        Assert.assertEquals(project.getDescription(), deleteProject.getDescription());
    }

    @Test
    @TestCaseName("Run testDeleteById for deleteById({0}, {1})")
    @Category({PositiveImplementation.class, RepositoryImplementation.class})
    @Parameters(
            source = CaseDataProjectProvider.class,
            method = "validProjectsCaseData"
    )
    public void testDeleteById(
            @NotNull final User user,
            @NotNull final Project project
    ) {
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        Assert.assertNotNull(this.projectRepository);
        @NotNull final Project addRecord = this.projectRepository.addRecord(project);
        Assert.assertNotNull(addRecord);

        @Nullable final Project deleteProject = this.projectRepository.deleteById(user.getId(), project.getId());
        Assert.assertNotNull(deleteProject);
        Assert.assertEquals(project.getId(), deleteProject.getId());
        Assert.assertEquals(project.getUserId(), deleteProject.getUserId());
        Assert.assertEquals(project.getTitle(), deleteProject.getTitle());
        Assert.assertEquals(project.getDescription(), deleteProject.getDescription());
    }

    @Test
    @TestCaseName("Run testDeleteByTitle for deleteByTitle({0}, {1})")
    @Category({PositiveImplementation.class, RepositoryImplementation.class})
    @Parameters(
            source = CaseDataProjectProvider.class,
            method = "validProjectsCaseData"
    )
    public void testDeleteByTitle(
            @NotNull final User user,
            @NotNull final Project project
    ) {
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        Assert.assertNotNull(this.projectRepository);
        @NotNull final Project addRecord = this.projectRepository.addRecord(project);
        Assert.assertNotNull(addRecord);

        @Nullable final Project deleteProject = this.projectRepository.deleteByTitle(user.getId(), project.getTitle());
        Assert.assertNotNull(deleteProject);
        Assert.assertEquals(project.getId(), deleteProject.getId());
        Assert.assertEquals(project.getUserId(), deleteProject.getUserId());
        Assert.assertEquals(project.getTitle(), deleteProject.getTitle());
        Assert.assertEquals(project.getDescription(), deleteProject.getDescription());
    }

    @Test
    @TestCaseName("Run testDeleteAll for deleteAll({0})")
    @Category({PositiveImplementation.class, RepositoryImplementation.class})
    @Parameters(
            source = CaseDataProjectProvider.class,
            method = "validProjectsCaseData"
    )
    public void testDeleteAll(
            @NotNull final User user,
            @NotNull final Project project
    ) {
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        Assert.assertNotNull(this.projectRepository);
        @NotNull final Project addRecord = this.projectRepository.addRecord(project);
        Assert.assertNotNull(addRecord);

        @Nullable final Collection<Project> deleteProjects = this.projectRepository.deleteAll(user.getId());
        Assert.assertNotNull(deleteProjects);
        Assert.assertNotEquals(0, deleteProjects.size());
        final boolean isUserTasks = deleteProjects.stream().allMatch(entity -> user.getId().equals(entity.getUserId()));
        Assert.assertTrue(isUserTasks);
    }

    @Test
    @TestCaseName("Run testGetAll for getAll({0})")
    @Category({PositiveImplementation.class, RepositoryImplementation.class})
    @Parameters(
            source = CaseDataProjectProvider.class,
            method = "validProjectsCaseData"
    )
    public void testGetAll(
            @NotNull final User user,
            @NotNull final Project project
    ) {
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        Assert.assertNotNull(this.projectRepository);
        @NotNull final Project addRecord = this.projectRepository.addRecord(project);
        Assert.assertNotNull(addRecord);

        @Nullable final Collection<Project> getProjects = this.projectRepository.getAll(user.getId());
        Assert.assertNotNull(getProjects);
        Assert.assertNotEquals(0, getProjects.size());
        final boolean isUserTasks = getProjects.stream().allMatch(entity -> user.getId().equals(entity.getUserId()));
        Assert.assertTrue(isUserTasks);
    }

    @Test
    @TestCaseName("Run testGetByIndex for getByIndex({0}, 0)")
    @Category({PositiveImplementation.class, RepositoryImplementation.class})
    @Parameters(
            source = CaseDataProjectProvider.class,
            method = "validProjectsCaseData"
    )
    public void testGetByIndex(
            @NotNull final User user,
            @NotNull final Project project
    ) {
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        Assert.assertNotNull(this.projectRepository);
        @NotNull final Project addRecord = this.projectRepository.addRecord(project);
        Assert.assertNotNull(addRecord);

        @Nullable final Project getProject = this.projectRepository.getByIndex(user.getId(), 0);
        Assert.assertNotNull(getProject);
        Assert.assertEquals(project.getId(), getProject.getId());
        Assert.assertEquals(project.getUserId(), getProject.getUserId());
        Assert.assertEquals(project.getTitle(), getProject.getTitle());
        Assert.assertEquals(project.getDescription(), getProject.getDescription());
    }

    @Test
    @TestCaseName("Run testGetById for getById({0}, {1})")
    @Category({PositiveImplementation.class, RepositoryImplementation.class})
    @Parameters(
            source = CaseDataProjectProvider.class,
            method = "validProjectsCaseData"
    )
    public void testGetById(
            @NotNull final User user,
            @NotNull final Project project
    ) {
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        Assert.assertNotNull(this.projectRepository);
        @NotNull final Project addRecord = this.projectRepository.addRecord(project);
        Assert.assertNotNull(addRecord);

        @Nullable final Project getProject = this.projectRepository.getById(user.getId(), project.getId());
        Assert.assertNotNull(getProject);
        Assert.assertEquals(project.getId(), getProject.getId());
        Assert.assertEquals(project.getUserId(), getProject.getUserId());
        Assert.assertEquals(project.getTitle(), getProject.getTitle());
        Assert.assertEquals(project.getDescription(), getProject.getDescription());
    }

    @Test
    @TestCaseName("Run testGetByTitle for getByTitle({0}, {1})")
    @Category({PositiveImplementation.class, RepositoryImplementation.class})
    @Parameters(
            source = CaseDataProjectProvider.class,
            method = "validProjectsCaseData"
    )
    public void testGetByTitle(
            @NotNull final User user,
            @NotNull final Project project
    ) {
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        Assert.assertNotNull(this.projectRepository);
        @NotNull final Project addRecord = this.projectRepository.addRecord(project);
        Assert.assertNotNull(addRecord);

        @Nullable final Project getProject = this.projectRepository.getByTitle(user.getId(), project.getTitle());
        Assert.assertNotNull(getProject);
        Assert.assertEquals(project.getId(), getProject.getId());
        Assert.assertEquals(project.getUserId(), getProject.getUserId());
        Assert.assertEquals(project.getTitle(), getProject.getTitle());
        Assert.assertEquals(project.getDescription(), getProject.getDescription());
    }

}