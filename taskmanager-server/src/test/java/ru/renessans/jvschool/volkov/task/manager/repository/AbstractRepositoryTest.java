package ru.renessans.jvschool.volkov.task.manager.repository;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.naming.TestCaseName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.casedata.CaseDataBaseProvider;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.RepositoryImplementation;
import ru.renessans.jvschool.volkov.task.manager.model.AbstractModel;
import ru.renessans.jvschool.volkov.task.manager.util.HashUtil;

import java.util.Collection;
import java.util.Collections;

@RunWith(value = JUnitParamsRunner.class)
public final class AbstractRepositoryTest {

    private static final class AbstractTestableRepository extends AbstractRepository<AbstractTestableModel> {
    }

    @Getter
    @Setter
    @AllArgsConstructor
    private static final class AbstractTestableModel extends AbstractModel {

        @Nullable
        private String value;

    }

    @NotNull
    private final AbstractRepository<AbstractTestableModel> abstractRepository = new AbstractTestableRepository();

    @Test
    @TestCaseName("Run testAddRecord for addRecord(value)")
    @Category({PositiveImplementation.class, RepositoryImplementation.class})
    @Parameters(
            source = CaseDataBaseProvider.class,
            method = "validLinesCaseData"
    )
    public void testAddRecord(
            @NotNull final String value
    ) {
        Assert.assertNotNull(this.abstractRepository);
        Assert.assertNotNull(value);
        @NotNull final AbstractTestableModel model = new AbstractTestableModel(value);
        Assert.assertNotNull(model);

        @NotNull final AbstractTestableModel addRecord = this.abstractRepository.addRecord(model);
        Assert.assertNotNull(addRecord);
        Assert.assertEquals(model.getId(), addRecord.getId());
        Assert.assertEquals(model.getValue(), addRecord.getValue());
    }

    @Test
    @TestCaseName("Run testUpdateRecord for updateRecord(value)")
    @Category({PositiveImplementation.class, RepositoryImplementation.class})
    @Parameters(
            source = CaseDataBaseProvider.class,
            method = "validLinesCaseData"
    )
    public void testUpdateRecord(
            @NotNull final String value
    ) {
        Assert.assertNotNull(this.abstractRepository);
        Assert.assertNotNull(value);
        @NotNull final AbstractTestableModel model = new AbstractTestableModel(value);
        Assert.assertNotNull(model);
        @NotNull final AbstractTestableModel addRecord = this.abstractRepository.addRecord(model);
        Assert.assertNotNull(addRecord);

        @NotNull final String newValue = HashUtil.getHashLine(value);
        Assert.assertNotNull(newValue);
        model.setValue(newValue);
        @Nullable final AbstractTestableModel updateModel = this.abstractRepository.updateRecord(model);
        Assert.assertNotNull(updateModel);
        Assert.assertEquals(model.getId(), updateModel.getId());
    }

    @Test
    @TestCaseName("Run testGetAllRecords for getAllRecords()")
    @Category({PositiveImplementation.class, RepositoryImplementation.class})
    @Parameters(
            source = CaseDataBaseProvider.class,
            method = "validLinesCaseData"
    )
    public void testGetAllRecords(
            @NotNull final String value
    ) {
        Assert.assertNotNull(this.abstractRepository);
        Assert.assertNotNull(value);
        @NotNull final AbstractTestableModel model = new AbstractTestableModel(value);
        Assert.assertNotNull(model);
        @NotNull final AbstractTestableModel addRecord = this.abstractRepository.addRecord(model);
        Assert.assertNotNull(addRecord);

        @NotNull final Collection<AbstractTestableModel> getModels = this.abstractRepository.getAllRecords();
        Assert.assertNotNull(getModels);
        Assert.assertNotEquals(0, getModels.size());
    }

    @Test
    @TestCaseName("Run testGetRecordByKey for getRecordByKey(key)")
    @Category({PositiveImplementation.class, RepositoryImplementation.class})
    @Parameters(
            source = CaseDataBaseProvider.class,
            method = "validLinesCaseData"
    )
    public void testGetRecordByKey(
            @NotNull final String value
    ) {
        Assert.assertNotNull(this.abstractRepository);
        Assert.assertNotNull(value);
        @NotNull final AbstractTestableModel model = new AbstractTestableModel(value);
        Assert.assertNotNull(model);
        @NotNull final AbstractTestableModel addRecord = this.abstractRepository.addRecord(model);
        Assert.assertNotNull(addRecord);

        @Nullable final AbstractTestableModel getModel = this.abstractRepository.getRecordByKey(model.getId());
        Assert.assertNotNull(getModel);
        Assert.assertEquals(model.getId(), getModel.getId());
        Assert.assertEquals(model.getValue(), getModel.getValue());
    }

    @Test
    @TestCaseName("Run testDeleteAllRecords for deleteAllRecords()")
    @Category({PositiveImplementation.class, RepositoryImplementation.class})
    @Parameters(
            source = CaseDataBaseProvider.class,
            method = "validLinesCaseData"
    )
    public void testDeleteAllRecords(
            @NotNull final String value
    ) {
        Assert.assertNotNull(this.abstractRepository);
        Assert.assertNotNull(value);
        @NotNull final AbstractTestableModel model = new AbstractTestableModel(value);
        Assert.assertNotNull(model);
        @NotNull final AbstractTestableModel addRecord = this.abstractRepository.addRecord(model);
        Assert.assertNotNull(addRecord);

        @NotNull final Collection<AbstractTestableModel> deleteAllModels = this.abstractRepository.deleteAllRecords();
        Assert.assertNotNull(deleteAllModels);
    }

    @Test
    @TestCaseName("Run testDeleteRecordByKey for deleteRecordByKey(key)")
    @Category({PositiveImplementation.class, RepositoryImplementation.class})
    @Parameters(
            source = CaseDataBaseProvider.class,
            method = "validLinesCaseData"
    )
    public void testDeleteRecordByKey(
            @NotNull final String value
    ) {
        Assert.assertNotNull(this.abstractRepository);
        Assert.assertNotNull(value);
        @NotNull final AbstractTestableModel model = new AbstractTestableModel(value);
        Assert.assertNotNull(model);
        @NotNull final AbstractTestableModel addRecord = this.abstractRepository.addRecord(model);
        Assert.assertNotNull(addRecord);

        @Nullable final AbstractTestableModel deleteModel = this.abstractRepository.deleteRecordByKey(model.getId());
        Assert.assertNotNull(deleteModel);
        Assert.assertEquals(model.getId(), deleteModel.getId());
        Assert.assertEquals(model.getValue(), deleteModel.getValue());
    }

    @Test
    @TestCaseName("Run testDeleteRecord for deleteRecord(value)")
    @Category({PositiveImplementation.class, RepositoryImplementation.class})
    @Parameters(
            source = CaseDataBaseProvider.class,
            method = "validLinesCaseData"
    )
    public void testDeleteRecord(
            @NotNull final String value
    ) {
        Assert.assertNotNull(this.abstractRepository);
        Assert.assertNotNull(value);
        @NotNull final AbstractTestableModel model = new AbstractTestableModel(value);
        Assert.assertNotNull(model);
        @NotNull final AbstractTestableModel addModel = this.abstractRepository.addRecord(model);
        Assert.assertNotNull(addModel);

        @Nullable final AbstractTestableModel deleteModel = this.abstractRepository.deleteRecord(model);
        Assert.assertNotNull(deleteModel);
        Assert.assertEquals(model.getId(), deleteModel.getId());
        Assert.assertEquals(model.getValue(), deleteModel.getValue());
    }

    @Test
    @TestCaseName("Run testSetAllRecords for setAbstractTestableCollection(values)")
    @Category({PositiveImplementation.class, RepositoryImplementation.class})
    @Parameters(
            source = CaseDataBaseProvider.class,
            method = "validLinesCaseData"
    )
    public void testSetAllRecords(
            @NotNull final String value
    ) {
        Assert.assertNotNull(this.abstractRepository);
        Assert.assertNotNull(value);
        @NotNull final AbstractTestableModel model = new AbstractTestableModel(value);
        Assert.assertNotNull(model);
        @NotNull final Collection<AbstractTestableModel> models = Collections.singletonList(model);
        Assert.assertNotNull(models);

        @NotNull final Collection<AbstractTestableModel> setAbstractTestableCollection =
                this.abstractRepository.setAllRecords(models);
        Assert.assertNotNull(setAbstractTestableCollection);
        Assert.assertNotEquals(0, setAbstractTestableCollection.size());
        Assert.assertEquals(models, setAbstractTestableCollection);
    }

    @Test
    @TestCaseName("Run testDeletedRecord for deletedRecord(value)")
    @Category({PositiveImplementation.class, RepositoryImplementation.class})
    @Parameters(
            source = CaseDataBaseProvider.class,
            method = "validLinesCaseData"
    )
    public void testDeletedRecord(
            @NotNull final String value
    ) {
        Assert.assertNotNull(this.abstractRepository);
        Assert.assertNotNull(value);
        @NotNull final AbstractTestableModel model = new AbstractTestableModel(value);
        Assert.assertNotNull(model);
        @NotNull final AbstractTestableModel addModel = this.abstractRepository.addRecord(model);
        Assert.assertNotNull(addModel);

        final boolean isDeletedModel = this.abstractRepository.deletedRecord(model);
        Assert.assertTrue(isDeletedModel);
    }

    @Test
    @TestCaseName("Run testDeletedRecords for deletedRecords()")
    @Category({PositiveImplementation.class, RepositoryImplementation.class})
    @Parameters(
            source = CaseDataBaseProvider.class,
            method = "validLinesCaseData"
    )
    public void testDeletedRecords(
            @NotNull final String value
    ) {
        Assert.assertNotNull(this.abstractRepository);
        Assert.assertNotNull(value);
        @NotNull final AbstractTestableModel model = new AbstractTestableModel(value);
        Assert.assertNotNull(model);
        @NotNull final Collection<AbstractTestableModel> models = Collections.singletonList(model);
        Assert.assertNotNull(models);
        @NotNull final Collection<AbstractTestableModel> setAbstractTestableCollection =
                this.abstractRepository.setAllRecords(models);
        Assert.assertNotNull(setAbstractTestableCollection);

        final boolean isDeletedModels = this.abstractRepository.deletedRecords();
        Assert.assertTrue(isDeletedModels);
    }

}