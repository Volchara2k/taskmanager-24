package ru.renessans.jvschool.volkov.task.manager.endpoint;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.api.IServiceLocatorRepository;
import ru.renessans.jvschool.volkov.task.manager.api.IServiceLocatorService;
import ru.renessans.jvschool.volkov.task.manager.api.endpoint.IProjectEndpoint;
import ru.renessans.jvschool.volkov.task.manager.api.service.*;
import ru.renessans.jvschool.volkov.task.manager.casedata.CaseDataProjectProvider;
import ru.renessans.jvschool.volkov.task.manager.marker.EndpointImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.model.Project;
import ru.renessans.jvschool.volkov.task.manager.model.Session;
import ru.renessans.jvschool.volkov.task.manager.model.User;
import ru.renessans.jvschool.volkov.task.manager.repository.ServiceLocatorRepository;
import ru.renessans.jvschool.volkov.task.manager.service.ServiceLocatorService;

import java.util.Collection;
import java.util.UUID;

@RunWith(value = JUnitParamsRunner.class)
@Category({PositiveImplementation.class, EndpointImplementation.class})
public final class ProjectEndpointTest {

    @NotNull
    private final IServiceLocatorRepository serviceLocatorRepository = new ServiceLocatorRepository();

    @NotNull
    private final IServiceLocatorService serviceLocator = new ServiceLocatorService(serviceLocatorRepository);

    @NotNull
    private final IUserService userService = serviceLocator.getUserService();

    @NotNull
    private final IProjectUserService projectService = serviceLocator.getProjectService();

    @NotNull
    private final IConfigurationService configService = serviceLocator.getConfigurationService();

    @NotNull
    private final ISessionService sessionService = serviceLocator.getSessionService();

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(serviceLocator);

    @Before
    public void loadConfigurationBefore() {
        Assert.assertNotNull(this.configService);
        this.configService.load();
    }

    @Test
    @TestCaseName("Run testAddProject for add(session, {0}, {1})")
    @Parameters(
            source = CaseDataProjectProvider.class,
            method = "validProjectsCaseData"
    )
    public void testAddProject(
            @NotNull final User user,
            @NotNull final Project project
    ) {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocator);
        Assert.assertNotNull(this.projectEndpoint);
        Assert.assertNotNull(this.projectService);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.sessionService);
        @NotNull final User addRecord = this.userService.addUser(user.getLogin(), user.getPasswordHash());
        Assert.assertNotNull(addRecord);
        @NotNull final Session open = this.sessionService.openSession(
                user.getLogin(), user.getPasswordHash()
        );
        Assert.assertNotNull(open);

        @NotNull final Project addProject = this.projectEndpoint.addProject(
                open, project.getTitle(), project.getDescription()
        );
        Assert.assertNotNull(addProject);
        Assert.assertEquals(addRecord.getId(), addProject.getUserId());
        Assert.assertEquals(project.getTitle(), addProject.getTitle());
        Assert.assertEquals(project.getDescription(), addProject.getDescription());
    }

    @Test
    @TestCaseName("Run testUpdateByIndex for updateByIndex({0}, 0, {1})")
    @Parameters(
            source = CaseDataProjectProvider.class,
            method = "validProjectsCaseData"
    )
    public void testUpdateProjectByIndex(
            @NotNull final User user,
            @NotNull final Project project
    ) {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocator);
        Assert.assertNotNull(this.projectEndpoint);
        Assert.assertNotNull(this.projectService);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.sessionService);
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        @NotNull final User addRecord = this.userService.addUser(user.getLogin(), user.getPasswordHash());
        Assert.assertNotNull(addRecord);
        @NotNull final Session open = this.sessionService.openSession(
                user.getLogin(), user.getPasswordHash()
        );
        Assert.assertNotNull(open);
        @NotNull final Project addProjectRecord = this.projectService.add(
                addRecord.getId(), project.getTitle(), project.getDescription()
        );
        Assert.assertNotNull(addProjectRecord);

        @NotNull final String newDescription = UUID.randomUUID().toString();
        Assert.assertNotNull(newDescription);
        project.setTitle(newDescription);
        @Nullable final Project updateProject =
                this.projectEndpoint.updateProjectByIndex(open, 0, project.getTitle(), newDescription);
        Assert.assertNotNull(updateProject);
        Assert.assertEquals(addProjectRecord.getId(), updateProject.getId());
        Assert.assertEquals(addProjectRecord.getUserId(), updateProject.getUserId());
        Assert.assertEquals(project.getTitle(), updateProject.getTitle());
    }

    @Test
    @TestCaseName("Run testUpdateProjectById for updateByIndex({0}, 0, {1})")
    @Parameters(
            source = CaseDataProjectProvider.class,
            method = "validProjectsCaseData"
    )
    public void testUpdateProjectById(
            @NotNull final User user,
            @NotNull final Project project
    ) {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocator);
        Assert.assertNotNull(this.projectEndpoint);
        Assert.assertNotNull(this.projectService);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.sessionService);
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        @NotNull final User addRecord = this.userService.addUser(user.getLogin(), user.getPasswordHash());
        Assert.assertNotNull(addRecord);
        @NotNull final Project addProjectRecord = this.projectService.add(
                addRecord.getId(), project.getTitle(), project.getDescription()
        );
        Assert.assertNotNull(addProjectRecord);
        @NotNull final Session open = this.sessionService.openSession(
                user.getLogin(), user.getPasswordHash()
        );
        Assert.assertNotNull(open);

        @NotNull final String newDescription = UUID.randomUUID().toString();
        Assert.assertNotNull(newDescription);
        project.setTitle(newDescription);
        @Nullable final Project updateProject = this.projectEndpoint.updateProjectById(
                open, addProjectRecord.getId(), project.getTitle(), newDescription
        );
        Assert.assertNotNull(updateProject);
        Assert.assertEquals(addProjectRecord.getId(), updateProject.getId());
        Assert.assertEquals(addProjectRecord.getUserId(), updateProject.getUserId());
        Assert.assertEquals(project.getTitle(), updateProject.getTitle());
    }

    @Test
    @TestCaseName("Run testDeleteProjectById for updateByIndex({0}, 0, {1})")
    @Parameters(
            source = CaseDataProjectProvider.class,
            method = "validProjectsCaseData"
    )
    public void testDeleteProjectById(
            @NotNull final User user,
            @NotNull final Project project
    ) {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocator);
        Assert.assertNotNull(this.projectEndpoint);
        Assert.assertNotNull(this.projectService);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.sessionService);
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        @NotNull final User addRecord = this.userService.addUser(user.getLogin(), user.getPasswordHash());
        Assert.assertNotNull(addRecord);
        @NotNull final Project addProjectRecord = this.projectService.add(
                addRecord.getId(), project.getTitle(), project.getDescription()
        );
        Assert.assertNotNull(addProjectRecord);
        @NotNull final Session open = this.sessionService.openSession(
                user.getLogin(), user.getPasswordHash()
        );
        Assert.assertNotNull(open);

        @Nullable final Project deleteProject = this.projectEndpoint.deleteProjectById(open, addProjectRecord.getId());
        Assert.assertNotNull(deleteProject);
        Assert.assertEquals(addProjectRecord.getId(), deleteProject.getId());
        Assert.assertEquals(addProjectRecord.getUserId(), deleteProject.getUserId());
        Assert.assertEquals(project.getTitle(), deleteProject.getTitle());
        Assert.assertEquals(project.getDescription(), deleteProject.getDescription());
    }

    @Test
    @TestCaseName("Run testDeleteProjectByIndex for updateByIndex({0}, 0, {1})")
    @Parameters(
            source = CaseDataProjectProvider.class,
            method = "validProjectsCaseData"
    )
    public void testDeleteProjectByIndex(
            @NotNull final User user,
            @NotNull final Project project
    ) {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocator);
        Assert.assertNotNull(this.projectEndpoint);
        Assert.assertNotNull(this.projectService);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.sessionService);
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        @NotNull final User addRecord = this.userService.addUser(user.getLogin(), user.getPasswordHash());
        Assert.assertNotNull(addRecord);
        @NotNull final Project addProjectRecord = this.projectService.add(
                addRecord.getId(), project.getTitle(), project.getDescription()
        );
        Assert.assertNotNull(addProjectRecord);
        @NotNull final Session open = this.sessionService.openSession(
                user.getLogin(), user.getPasswordHash()
        );
        Assert.assertNotNull(open);

        @Nullable final Project deleteProject = this.projectEndpoint.deleteProjectByIndex(open, 0);
        Assert.assertNotNull(deleteProject);
        Assert.assertEquals(addProjectRecord.getId(), deleteProject.getId());
        Assert.assertEquals(addProjectRecord.getUserId(), deleteProject.getUserId());
        Assert.assertEquals(project.getTitle(), deleteProject.getTitle());
        Assert.assertEquals(project.getDescription(), deleteProject.getDescription());
    }

    @Test
    @TestCaseName("Run testDeleteProjectByTitle for updateByIndex({0}, 0, {1})")
    @Parameters(
            source = CaseDataProjectProvider.class,
            method = "validProjectsCaseData"
    )
    public void testDeleteProjectByTitle(
            @NotNull final User user,
            @NotNull final Project project
    ) {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocator);
        Assert.assertNotNull(this.projectEndpoint);
        Assert.assertNotNull(this.projectService);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.sessionService);
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        @NotNull final User addRecord = this.userService.addUser(user.getLogin(), user.getPasswordHash());
        Assert.assertNotNull(addRecord);
        @NotNull final Project addProjectRecord = this.projectService.add(
                addRecord.getId(), project.getTitle(), project.getDescription()
        );
        Assert.assertNotNull(addProjectRecord);
        @NotNull final Session open = this.sessionService.openSession(
                user.getLogin(), user.getPasswordHash()
        );
        Assert.assertNotNull(open);

        @Nullable final Project deleteProject = this.projectEndpoint.deleteProjectByTitle(open, addProjectRecord.getTitle());
        Assert.assertNotNull(deleteProject);
        Assert.assertEquals(addProjectRecord.getId(), deleteProject.getId());
        Assert.assertEquals(addProjectRecord.getUserId(), deleteProject.getUserId());
        Assert.assertEquals(project.getTitle(), deleteProject.getTitle());
        Assert.assertEquals(project.getDescription(), deleteProject.getDescription());
    }

    @Test
    @TestCaseName("Run testDeleteAllProjects for deleteAllProjects({0}, 0, {1})")
    @Parameters(
            source = CaseDataProjectProvider.class,
            method = "validProjectsCaseData"
    )
    public void testDeleteAllProjects(
            @NotNull final User user,
            @NotNull final Project project
    ) {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocator);
        Assert.assertNotNull(this.projectEndpoint);
        Assert.assertNotNull(this.projectService);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.sessionService);
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        @NotNull final User addRecord = this.userService.addUser(user.getLogin(), user.getPasswordHash());
        Assert.assertNotNull(addRecord);
        @NotNull final Project addProjectRecord = this.projectService.add(
                addRecord.getId(), project.getTitle(), project.getDescription()
        );
        Assert.assertNotNull(addProjectRecord);
        @NotNull final Session open = this.sessionService.openSession(
                user.getLogin(), user.getPasswordHash()
        );
        Assert.assertNotNull(open);

        @Nullable final Collection<Project> deleteProjects = this.projectEndpoint.deleteAllProjects(open);
        Assert.assertNotNull(deleteProjects);
        Assert.assertNotEquals(0, deleteProjects.size());
        final boolean isUserTasks = deleteProjects.stream().allMatch(entity -> addRecord.getId().equals(entity.getUserId()));
        Assert.assertTrue(isUserTasks);
    }

    @Test
    @TestCaseName("Run testGetProjectById for getProjectById({0}, 0, {1})")
    @Parameters(
            source = CaseDataProjectProvider.class,
            method = "validProjectsCaseData"
    )
    public void testGetProjectById(
            @NotNull final User user,
            @NotNull final Project project
    ) {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocator);
        Assert.assertNotNull(this.projectEndpoint);
        Assert.assertNotNull(this.projectService);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.sessionService);
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        @NotNull final User addRecord = this.userService.addUser(user.getLogin(), user.getPasswordHash());
        Assert.assertNotNull(addRecord);
        @NotNull final Project addProjectRecord = this.projectService.add(
                addRecord.getId(), project.getTitle(), project.getDescription()
        );
        Assert.assertNotNull(addProjectRecord);
        @NotNull final Session open = this.sessionService.openSession(
                user.getLogin(), user.getPasswordHash()
        );
        Assert.assertNotNull(open);

        @Nullable final Project getProject = this.projectEndpoint.getProjectById(open, addProjectRecord.getId());
        Assert.assertNotNull(getProject);
        Assert.assertEquals(addProjectRecord.getId(), getProject.getId());
        Assert.assertEquals(addProjectRecord.getUserId(), getProject.getUserId());
        Assert.assertEquals(project.getTitle(), getProject.getTitle());
        Assert.assertEquals(project.getDescription(), getProject.getDescription());
    }

    @Test
    @TestCaseName("Run testGetProjectByIndex for updateByIndex({0}, 0, {1})")
    @Parameters(
            source = CaseDataProjectProvider.class,
            method = "validProjectsCaseData"
    )
    public void testGetProjectByIndex(
            @NotNull final User user,
            @NotNull final Project project
    ) {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocator);
        Assert.assertNotNull(this.projectEndpoint);
        Assert.assertNotNull(this.projectService);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.sessionService);
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        @NotNull final User addRecord = this.userService.addUser(user.getLogin(), user.getPasswordHash());
        Assert.assertNotNull(addRecord);
        @NotNull final Project addProjectRecord = this.projectService.add(
                addRecord.getId(), project.getTitle(), project.getDescription()
        );
        Assert.assertNotNull(addProjectRecord);
        @NotNull final Session open = this.sessionService.openSession(
                user.getLogin(), user.getPasswordHash()
        );
        Assert.assertNotNull(open);

        @Nullable final Project getProject = this.projectEndpoint.getProjectByIndex(open, 0);
        Assert.assertNotNull(getProject);
        Assert.assertEquals(addProjectRecord.getId(), getProject.getId());
        Assert.assertEquals(addProjectRecord.getUserId(), getProject.getUserId());
        Assert.assertEquals(project.getTitle(), getProject.getTitle());
        Assert.assertEquals(project.getDescription(), getProject.getDescription());
    }

    @Test
    @TestCaseName("Run testDeleteProjectByTitle for updateByIndex({0}, 0, {1})")
    @Parameters(
            source = CaseDataProjectProvider.class,
            method = "validProjectsCaseData"
    )
    public void testGetProjectByTitle(
            @NotNull final User user,
            @NotNull final Project project
    ) {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocator);
        Assert.assertNotNull(this.projectEndpoint);
        Assert.assertNotNull(this.projectService);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.sessionService);
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        @NotNull final User addRecord = this.userService.addUser(user.getLogin(), user.getPasswordHash());
        Assert.assertNotNull(addRecord);
        @NotNull final Project addProjectRecord = this.projectService.add(
                addRecord.getId(), project.getTitle(), project.getDescription()
        );
        Assert.assertNotNull(addProjectRecord);
        @NotNull final Session open = this.sessionService.openSession(
                user.getLogin(), user.getPasswordHash()
        );
        Assert.assertNotNull(open);

        @Nullable final Project getProject = this.projectEndpoint.getProjectByTitle(open, addProjectRecord.getTitle());
        Assert.assertNotNull(getProject);
        Assert.assertEquals(addProjectRecord.getId(), getProject.getId());
        Assert.assertEquals(addProjectRecord.getUserId(), getProject.getUserId());
        Assert.assertEquals(project.getTitle(), getProject.getTitle());
        Assert.assertEquals(project.getDescription(), getProject.getDescription());
    }

    @Test
    @TestCaseName("Run testDeleteProjectByTitle for updateByIndex({0}, 0, {1})")
    @Parameters(
            source = CaseDataProjectProvider.class,
            method = "validProjectsCaseData"
    )
    public void testGetAllProjects(
            @NotNull final User user,
            @NotNull final Project project
    ) {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocator);
        Assert.assertNotNull(this.projectEndpoint);
        Assert.assertNotNull(this.projectService);
        Assert.assertNotNull(this.userService);
        Assert.assertNotNull(this.sessionService);
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        @NotNull final User addRecord = this.userService.addUser(user.getLogin(), user.getPasswordHash());
        Assert.assertNotNull(addRecord);
        @NotNull final Project addProjectRecord = this.projectService.add(
                addRecord.getId(), project.getTitle(), project.getDescription()
        );
        Assert.assertNotNull(addProjectRecord);
        @NotNull final Session open = this.sessionService.openSession(
                user.getLogin(), user.getPasswordHash()
        );
        Assert.assertNotNull(open);

        @Nullable final Collection<Project> getAllProjects = this.projectEndpoint.getAllProjects(open);
        Assert.assertNotNull(getAllProjects);
        Assert.assertNotEquals(0, getAllProjects.size());
        final boolean isUserTasks = getAllProjects.stream().allMatch(entity -> addRecord.getId().equals(entity.getUserId()));
        Assert.assertTrue(isUserTasks);
    }

}