package ru.renessans.jvschool.volkov.task.manager.casedata;

import ru.renessans.jvschool.volkov.task.manager.constant.DemoDataConst;
import ru.renessans.jvschool.volkov.task.manager.dto.Domain;
import ru.renessans.jvschool.volkov.task.manager.model.Project;
import ru.renessans.jvschool.volkov.task.manager.model.Task;
import ru.renessans.jvschool.volkov.task.manager.model.User;
import ru.renessans.jvschool.volkov.task.manager.util.HashUtil;

import java.util.Arrays;
import java.util.Collections;

public final class CaseDomainProvider {

    @SuppressWarnings("unused")
    public Object[] validCollectionDomainsCaseData() {
        return new Object[]{
                new Object[]{
                        new Domain(
                                Arrays.asList(
                                        new Project(
                                                "demo",
                                                "fg6A0Induv"
                                        ),
                                        new Project()
                                ),
                                Arrays.asList(
                                        new Task(
                                                ".....",
                                                "JyWYAYmqJf"
                                        ),
                                        new Task()
                                ),
                                Arrays.asList(
                                        new User(
                                                HashUtil.getHashLine(DemoDataConst.USER_ADMIN_LOGIN),
                                                HashUtil.getHashLine(DemoDataConst.USER_ADMIN_PASSWORD)
                                        ),
                                        new User(
                                                "!23",
                                                "nnerVw24PV"
                                        ),
                                        new User()
                                )
                        )
                },
                new Object[]{
                        new Domain(
                                Collections.singletonList(
                                        new Project(
                                                "fg6A0Induv",
                                                "fg6A0Induv"
                                        )
                                ),
                                Collections.singletonList(
                                        new Task(
                                                "JyWYAYmqJf",
                                                "JyWYAYmqJf"
                                        )
                                ),
                                Collections.singletonList(
                                        new User(
                                                "nnerVw24PV",
                                                "nnerVw24PV"
                                        )
                                )
                        )
                },
                new Object[]{
                        new Domain(
                                Collections.singletonList(
                                        new Project(
                                                DemoDataConst.PROJECT_TITLE,
                                                DemoDataConst.PROJECT_DESCRIPTION
                                        )
                                ),
                                Collections.singletonList(
                                        new Task(
                                                DemoDataConst.TASK_TITLE,
                                                DemoDataConst.TASK_DESCRIPTION
                                        )
                                ),
                                Collections.singletonList(
                                        new User(
                                                DemoDataConst.USER_TEST_LOGIN,
                                                DemoDataConst.USER_TEST_PASSWORD
                                        )
                                )
                        )
                }
        };
    }

}