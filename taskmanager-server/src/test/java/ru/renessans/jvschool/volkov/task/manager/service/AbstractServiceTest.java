package ru.renessans.jvschool.volkov.task.manager.service;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.naming.TestCaseName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.api.IRepository;
import ru.renessans.jvschool.volkov.task.manager.casedata.CaseDataBaseProvider;
import ru.renessans.jvschool.volkov.task.manager.exception.empty.service.EmptyKeyException;
import ru.renessans.jvschool.volkov.task.manager.exception.empty.service.EmptyValueException;
import ru.renessans.jvschool.volkov.task.manager.exception.empty.service.EmptyValuesException;
import ru.renessans.jvschool.volkov.task.manager.marker.NegativeImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.ServiceImplementation;
import ru.renessans.jvschool.volkov.task.manager.model.AbstractModel;
import ru.renessans.jvschool.volkov.task.manager.repository.AbstractRepository;
import ru.renessans.jvschool.volkov.task.manager.util.HashUtil;

import java.util.Collection;
import java.util.Collections;

@RunWith(value = JUnitParamsRunner.class)
public final class AbstractServiceTest {

    private static final class AbstractTestableRepository extends AbstractRepository<AbstractTestableModel> {
    }

    private static final class AbstractTestableService extends AbstractService<AbstractTestableModel> {

        public AbstractTestableService(@NotNull final IRepository<AbstractTestableModel> repository) {
            super(repository);
        }

    }

    @Getter
    @Setter
    @AllArgsConstructor
    private static final class AbstractTestableModel extends AbstractModel {

        @Nullable
        private String value;

    }

    @NotNull
    private final AbstractRepository<AbstractTestableModel> abstractRepository = new AbstractTestableRepository();

    @NotNull
    private final AbstractService<AbstractTestableModel> abstractService = new AbstractTestableService(abstractRepository);

    @Test(expected = EmptyValueException.class)
    @TestCaseName("Run testAddNegativeRecord for addRecord(null)")
    @Category({NegativeImplementation.class, ServiceImplementation.class})
    public void testNegativeAddRecord() {
        Assert.assertNotNull(this.abstractRepository);
        Assert.assertNotNull(this.abstractService);
        this.abstractService.addRecord(null);
    }

    @Test(expected = EmptyValueException.class)
    @TestCaseName("Run testNegativeUpdateRecord for updateRecord(null)")
    @Category({NegativeImplementation.class, ServiceImplementation.class})
    public void testNegativeUpdateRecord() {
        Assert.assertNotNull(this.abstractRepository);
        Assert.assertNotNull(this.abstractService);
        this.abstractService.updateRecord(null);
    }

    @Test(expected = EmptyKeyException.class)
    @TestCaseName("Run testNegativeGetRecordByKey for getRecordByKey(key)")
    @Category({NegativeImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataBaseProvider.class,
            method = "invalidLinesCaseData"
    )
    public void testNegativeGetRecordByKey(
            @Nullable final String value
    ) {
        Assert.assertNotNull(this.abstractRepository);
        Assert.assertNotNull(this.abstractService);
        this.abstractService.getRecordByKey(value);
    }

    @Test(expected = EmptyKeyException.class)
    @TestCaseName("Run testNegativeDeleteRecordByKey for deleteRecordByKey(key)")
    @Category({NegativeImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataBaseProvider.class,
            method = "invalidLinesCaseData"
    )
    public void testNegativeDeleteRecordByKey(
            @Nullable final String value
    ) {
        Assert.assertNotNull(this.abstractRepository);
        Assert.assertNotNull(this.abstractService);
        this.abstractService.deleteRecordByKey(value);
    }

    @Test(expected = EmptyValueException.class)
    @TestCaseName("Run testNegativeDeleteRecord for deleteRecord(null)")
    @Category({NegativeImplementation.class, ServiceImplementation.class})
    public void testNegativeDeleteRecord() {
        Assert.assertNotNull(this.abstractRepository);
        Assert.assertNotNull(this.abstractService);
        this.abstractService.deleteRecord(null);
    }

    @Test(expected = EmptyValuesException.class)
    @TestCaseName("Run testNegativeSetAllRecords for setAllRecords(values)")
    @Category({NegativeImplementation.class, ServiceImplementation.class})
    public void testNegativeSetAllRecords() {
        Assert.assertNotNull(this.abstractRepository);
        Assert.assertNotNull(this.abstractService);
        this.abstractService.setAllRecords(null);
    }

    @Test(expected = EmptyValueException.class)
    @TestCaseName("Run testNegativeDeletedRecord for deletedRecord(null)")
    @Category({NegativeImplementation.class, ServiceImplementation.class})
    public void testNegativeDeletedRecord() {
        Assert.assertNotNull(this.abstractRepository);
        Assert.assertNotNull(this.abstractService);
        this.abstractService.deletedRecord(null);
    }

    @Test
    @TestCaseName("Run testAddRecord for addRecord(value)")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataBaseProvider.class,
            method = "validLinesCaseData"
    )
    public void testAddRecord(
            @NotNull final String value
    ) {
        Assert.assertNotNull(this.abstractRepository);
        Assert.assertNotNull(this.abstractService);
        Assert.assertNotNull(value);
        @NotNull final AbstractTestableModel model = new AbstractTestableModel(value);
        Assert.assertNotNull(model);

        @NotNull final AbstractTestableModel addRecord = this.abstractService.addRecord(model);
        Assert.assertNotNull(addRecord);
        Assert.assertEquals(model.getId(), addRecord.getId());
        Assert.assertEquals(model.getValue(), addRecord.getValue());
    }

    @Test
    @TestCaseName("Run testUpdateRecord for updateRecord(value)")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataBaseProvider.class,
            method = "validLinesCaseData"
    )
    public void testUpdateRecord(
            @NotNull final String value
    ) {
        Assert.assertNotNull(this.abstractRepository);
        Assert.assertNotNull(this.abstractService);
        Assert.assertNotNull(value);
        @NotNull final AbstractTestableModel model = new AbstractTestableModel(value);
        Assert.assertNotNull(model);
        @NotNull final AbstractTestableModel addRecord = this.abstractService.addRecord(model);
        Assert.assertNotNull(addRecord);

        @NotNull final String newValue = HashUtil.getHashLine(value);
        Assert.assertNotNull(newValue);
        model.setValue(newValue);
        @Nullable final AbstractTestableModel updateModel = this.abstractService.updateRecord(model);
        Assert.assertNotNull(updateModel);
        Assert.assertEquals(model.getId(), updateModel.getId());
    }

    @Test
    @TestCaseName("Run testGetAllRecords for getAllRecords()")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataBaseProvider.class,
            method = "validLinesCaseData"
    )
    public void testGetAllRecords(
            @NotNull final String value
    ) {
        Assert.assertNotNull(this.abstractRepository);
        Assert.assertNotNull(this.abstractService);
        Assert.assertNotNull(value);
        @NotNull final AbstractTestableModel model = new AbstractTestableModel(value);
        Assert.assertNotNull(model);
        @NotNull final AbstractTestableModel addRecord = this.abstractService.addRecord(model);
        Assert.assertNotNull(addRecord);

        @NotNull final Collection<AbstractTestableModel> getModels = this.abstractService.getAllRecords();
        Assert.assertNotNull(getModels);
        Assert.assertNotEquals(0, getModels.size());
    }

    @Test
    @TestCaseName("Run testGetRecordByKey for getRecordByKey(key)")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataBaseProvider.class,
            method = "validLinesCaseData"
    )
    public void testGetRecordByKey(
            @NotNull final String value
    ) {
        Assert.assertNotNull(this.abstractRepository);
        Assert.assertNotNull(this.abstractService);
        Assert.assertNotNull(value);
        @NotNull final AbstractTestableModel model = new AbstractTestableModel(value);
        Assert.assertNotNull(model);
        @NotNull final AbstractTestableModel addRecord = this.abstractService.addRecord(model);
        Assert.assertNotNull(addRecord);

        @Nullable final AbstractTestableModel getModel = this.abstractService.getRecordByKey(model.getId());
        Assert.assertNotNull(getModel);
        Assert.assertEquals(model.getId(), getModel.getId());
        Assert.assertEquals(model.getValue(), getModel.getValue());
    }

    @Test
    @TestCaseName("Run testDeleteAllRecords for deleteAllRecords()")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataBaseProvider.class,
            method = "validLinesCaseData"
    )
    public void testDeleteAllRecords(
            @NotNull final String value
    ) {
        Assert.assertNotNull(this.abstractRepository);
        Assert.assertNotNull(this.abstractService);
        Assert.assertNotNull(value);
        @NotNull final AbstractTestableModel model = new AbstractTestableModel(value);
        Assert.assertNotNull(model);
        @NotNull final AbstractTestableModel addRecord = this.abstractService.addRecord(model);
        Assert.assertNotNull(addRecord);

        @NotNull final Collection<AbstractTestableModel> deleteAllModels = this.abstractService.deleteAllRecords();
        Assert.assertNotNull(deleteAllModels);
        Assert.assertNotEquals(0, deleteAllModels.size());
    }

    @Test
    @TestCaseName("Run testDeleteRecordByKey for deleteRecordByKey(key)")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataBaseProvider.class,
            method = "validLinesCaseData"
    )
    public void testDeleteRecordByKey(
            @NotNull final String value
    ) {
        Assert.assertNotNull(this.abstractRepository);
        Assert.assertNotNull(this.abstractService);
        Assert.assertNotNull(value);
        @NotNull final AbstractTestableModel model = new AbstractTestableModel(value);
        Assert.assertNotNull(model);
        @NotNull final AbstractTestableModel addRecord = this.abstractService.addRecord(model);
        Assert.assertNotNull(addRecord);

        @Nullable final AbstractTestableModel deleteModel = this.abstractService.deleteRecordByKey(model.getId());
        Assert.assertNotNull(deleteModel);
        Assert.assertEquals(model.getId(), deleteModel.getId());
        Assert.assertEquals(model.getValue(), deleteModel.getValue());
    }

    @Test
    @TestCaseName("Run testDeleteRecord for deleteRecord(value)")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataBaseProvider.class,
            method = "validLinesCaseData"
    )
    public void testDeleteRecord(
            @NotNull final String value
    ) {
        Assert.assertNotNull(this.abstractRepository);
        Assert.assertNotNull(this.abstractService);
        Assert.assertNotNull(value);
        Assert.assertNotNull(this.abstractRepository);
        Assert.assertNotNull(value);
        @NotNull final AbstractTestableModel model = new AbstractTestableModel(value);
        Assert.assertNotNull(model);
        @NotNull final AbstractTestableModel addModel = this.abstractService.addRecord(model);
        Assert.assertNotNull(addModel);

        @Nullable final AbstractTestableModel deleteModel = this.abstractService.deleteRecord(model);
        Assert.assertNotNull(deleteModel);
        Assert.assertEquals(model.getId(), deleteModel.getId());
        Assert.assertEquals(model.getValue(), deleteModel.getValue());
    }

    @Test
    @TestCaseName("Run testSetAllRecords for setAllRecords(values)")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataBaseProvider.class,
            method = "validLinesCaseData"
    )
    public void testSetAllRecords(
            @NotNull final String value
    ) {
        Assert.assertNotNull(this.abstractRepository);
        Assert.assertNotNull(this.abstractService);
        Assert.assertNotNull(value);
        @NotNull final AbstractTestableModel model = new AbstractTestableModel(value);
        Assert.assertNotNull(model);
        @NotNull final Collection<AbstractTestableModel> models = Collections.singletonList(model);
        Assert.assertNotNull(models);

        @NotNull final Collection<AbstractTestableModel> setAbstractTestableCollection =
                this.abstractService.setAllRecords(models);
        Assert.assertNotNull(setAbstractTestableCollection);
        Assert.assertNotEquals(0, setAbstractTestableCollection.size());
        Assert.assertEquals(models, setAbstractTestableCollection);
    }

    @Test
    @TestCaseName("Run testDeletedRecord for deletedRecord(value)")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataBaseProvider.class,
            method = "validLinesCaseData"
    )
    public void testDeletedRecord(
            @NotNull final String value
    ) {
        Assert.assertNotNull(this.abstractRepository);
        Assert.assertNotNull(this.abstractService);
        Assert.assertNotNull(value);
        @NotNull final AbstractTestableModel model = new AbstractTestableModel(value);
        Assert.assertNotNull(model);
        @NotNull final AbstractTestableModel addModel = this.abstractService.addRecord(model);
        Assert.assertNotNull(addModel);

        final boolean isDeletedModel = this.abstractService.deletedRecord(model);
        Assert.assertTrue(isDeletedModel);
    }

    @Test
    @TestCaseName("Run testDeletedRecords for deletedRecords()")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataBaseProvider.class,
            method = "validLinesCaseData"
    )
    public void testDeletedRecords(
            @NotNull final String value
    ) {
        Assert.assertNotNull(this.abstractRepository);
        Assert.assertNotNull(this.abstractService);
        Assert.assertNotNull(value);
        @NotNull final AbstractTestableModel model = new AbstractTestableModel(value);
        Assert.assertNotNull(model);
        @NotNull final Collection<AbstractTestableModel> models = Collections.singletonList(model);
        Assert.assertNotNull(models);
        @NotNull final Collection<AbstractTestableModel> setAbstractTestableCollection =
                this.abstractService.setAllRecords(models);
        Assert.assertNotNull(setAbstractTestableCollection);

        final boolean isDeletedModels = this.abstractService.deletedRecords();
        Assert.assertTrue(isDeletedModels);
    }

}