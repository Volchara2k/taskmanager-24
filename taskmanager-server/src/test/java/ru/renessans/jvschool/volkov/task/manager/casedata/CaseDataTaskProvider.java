package ru.renessans.jvschool.volkov.task.manager.casedata;

import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.constant.DemoDataConst;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRole;
import ru.renessans.jvschool.volkov.task.manager.model.Task;
import ru.renessans.jvschool.volkov.task.manager.model.User;
import ru.renessans.jvschool.volkov.task.manager.util.HashUtil;

public final class CaseDataTaskProvider {

    @SuppressWarnings("unused")
    public Object[] validTasksCaseData() {
        @Nullable User user;
        return new Object[]{
                new Object[]{
                        user = new User(
                                DemoDataConst.USER_TEST_LOGIN,
                                DemoDataConst.USER_TEST_PASSWORD
                        ),
                        new Task(
                                DemoDataConst.TASK_TITLE,
                                DemoDataConst.TASK_DESCRIPTION,
                                user.getId()
                        )
                },
                new Object[]{
                        user = new User(
                                DemoDataConst.USER_DEFAULT_LOGIN,
                                DemoDataConst.USER_DEFAULT_PASSWORD,
                                DemoDataConst.USER_DEFAULT_FIRSTNAME
                        ),
                        new Task(
                                HashUtil.getSaltHashLine(DemoDataConst.TASK_TITLE),
                                HashUtil.getSaltHashLine(DemoDataConst.TASK_DESCRIPTION),
                                user.getId()
                        )
                },
                new Object[]{
                        user = new User(
                                DemoDataConst.USER_ADMIN_LOGIN,
                                DemoDataConst.USER_ADMIN_PASSWORD,
                                UserRole.ADMIN
                        ),
                        new Task(
                                HashUtil.getHashLine(DemoDataConst.TASK_TITLE),
                                HashUtil.getHashLine(DemoDataConst.TASK_DESCRIPTION),
                                user.getId()
                        )
                }
        };
    }

}