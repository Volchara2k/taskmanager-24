package ru.renessans.jvschool.volkov.task.manager.command.admin.data.json;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEndpointLocatorService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceLocatorService;
import ru.renessans.jvschool.volkov.task.manager.command.AbstractCommand;
import ru.renessans.jvschool.volkov.task.manager.endpoint.AdminDataInterChangeEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.Domain;
import ru.renessans.jvschool.volkov.task.manager.endpoint.Session;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@SuppressWarnings("unused")
public final class DataJsonExportCommand extends AbstractCommand {

    @NotNull
    private static final String CMD_JSON_EXPORT = "data-json-export";

    @NotNull
    private static final String DESC_JSON_EXPORT = "экспортировать домен в json вид";

    @NotNull
    private static final String NOTIFY_JSON_EXPORT = "Происходит процесс выгрузки домена в json вид...";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_JSON_EXPORT;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_JSON_EXPORT;
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull final IServiceLocatorService serviceLocator = super.locatorService.getServiceLocator();
        @NotNull final ICurrentSessionService currentSessionService = serviceLocator.getCurrentSession();
        @Nullable final Session open = currentSessionService.getSession();

        @NotNull final IEndpointLocatorService endpointLocator = super.locatorService.getEndpointLocator();
        @NotNull final AdminDataInterChangeEndpoint dataEndpoint = endpointLocator.getAdminDataInterChangeEndpoint();

        @NotNull final Domain domain = dataEndpoint.exportDataJson(open);
        ViewUtil.print(NOTIFY_JSON_EXPORT);
        ViewUtil.print(domain.getUsers());
        ViewUtil.print(domain.getTasks());
        ViewUtil.print(domain.getProjects());
    }

}