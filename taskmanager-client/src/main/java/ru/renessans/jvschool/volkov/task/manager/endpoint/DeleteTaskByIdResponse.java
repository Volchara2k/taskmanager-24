
package ru.renessans.jvschool.volkov.task.manager.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for deleteTaskByIdResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="deleteTaskByIdResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="deletedTask" type="{http://endpoint.manager.task.volkov.jvschool.renessans.ru/}task" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "deleteTaskByIdResponse", propOrder = {
    "deletedTask"
})
public class DeleteTaskByIdResponse {

    protected Task deletedTask;

    /**
     * Gets the value of the deletedTask property.
     * 
     * @return
     *     possible object is
     *     {@link Task }
     *     
     */
    public Task getDeletedTask() {
        return deletedTask;
    }

    /**
     * Sets the value of the deletedTask property.
     * 
     * @param value
     *     allowed object is
     *     {@link Task }
     *     
     */
    public void setDeletedTask(Task value) {
        this.deletedTask = value;
    }

}
