package ru.renessans.jvschool.volkov.task.manager.exception.empty.service;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class EmptyLocatorException extends AbstractException {

    @NotNull
    private static final String EMPTY_LOCATOR = "Ошибка! Параметр \"служба веб-сервисов и сервисов\" является null!\n";

    public EmptyLocatorException() {
        super(EMPTY_LOCATOR);
    }

}