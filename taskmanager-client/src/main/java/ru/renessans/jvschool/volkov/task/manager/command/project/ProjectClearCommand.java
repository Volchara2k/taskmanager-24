package ru.renessans.jvschool.volkov.task.manager.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEndpointLocatorService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceLocatorService;
import ru.renessans.jvschool.volkov.task.manager.command.AbstractCommand;
import ru.renessans.jvschool.volkov.task.manager.endpoint.Project;
import ru.renessans.jvschool.volkov.task.manager.endpoint.ProjectEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.Session;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

import java.util.Collection;

@SuppressWarnings("unused")
public final class ProjectClearCommand extends AbstractCommand {

    @NotNull
    private static final String CMD_PROJECT_CLEAR = "project-clear";

    @NotNull
    private static final String DESC_PROJECT_CLEAR = "очистить все проекты";

    @NotNull
    private static final String NOTIFY_PROJECT_CLEAR = "Происходит попытка инициализации очистки списка проектов... ";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_PROJECT_CLEAR;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_PROJECT_CLEAR;
    }

    @Override
    public void execute() {
        @NotNull final IServiceLocatorService serviceLocator = super.locatorService.getServiceLocator();
        @NotNull final ICurrentSessionService currentSessionService = serviceLocator.getCurrentSession();
        @Nullable final Session open = currentSessionService.getSession();

        @NotNull final IEndpointLocatorService endpointLocator = super.locatorService.getEndpointLocator();
        @NotNull final ProjectEndpoint projectEndpoint = endpointLocator.getProjectEndpoint();
        @Nullable final Collection<Project> projects = projectEndpoint.deleteAllProjects(open);
        ViewUtil.print(NOTIFY_PROJECT_CLEAR);
        ViewUtil.print(projects);
    }

}