package ru.renessans.jvschool.volkov.task.manager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.endpoint.Session;

public interface ICurrentSessionService {

    @Nullable
    Session getSession();

    @NotNull
    Session subscribe(@Nullable Session session);

    Session unsubscribe();

}