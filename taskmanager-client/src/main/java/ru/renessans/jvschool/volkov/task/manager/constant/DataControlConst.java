package ru.renessans.jvschool.volkov.task.manager.constant;

import org.jetbrains.annotations.NotNull;

public interface DataControlConst {

    @NotNull
    String EXIT_FACTOR = "exit";

}