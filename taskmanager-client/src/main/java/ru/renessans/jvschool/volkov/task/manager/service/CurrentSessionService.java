package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.repository.ICurrentSessionRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.endpoint.Session;
import ru.renessans.jvschool.volkov.task.manager.exception.empty.owner.EmptySessionException;

import java.util.Objects;

@AllArgsConstructor
public final class CurrentSessionService implements ICurrentSessionService {

    @NotNull
    private final ICurrentSessionRepository sessionRepository;

    @Nullable
    @SneakyThrows
    @Override
    public Session getSession() {
        return this.sessionRepository.get();
    }

    @NotNull
    @SneakyThrows
    @Override
    public Session subscribe(
            @Nullable final Session session
    ) {
        if (Objects.isNull(session)) throw new EmptySessionException();
        return this.sessionRepository.put(session);
    }

    @SneakyThrows
    @Override
    public Session unsubscribe() {
        return this.sessionRepository.delete();
    }

}