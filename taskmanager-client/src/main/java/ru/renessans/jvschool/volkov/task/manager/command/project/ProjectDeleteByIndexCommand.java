package ru.renessans.jvschool.volkov.task.manager.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEndpointLocatorService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceLocatorService;
import ru.renessans.jvschool.volkov.task.manager.command.AbstractCommand;
import ru.renessans.jvschool.volkov.task.manager.endpoint.Project;
import ru.renessans.jvschool.volkov.task.manager.endpoint.ProjectEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.Session;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@SuppressWarnings("unused")
public final class ProjectDeleteByIndexCommand extends AbstractCommand {

    @NotNull
    private static final String CMD_PROJECT_DELETE_BY_INDEX = "project-delete-by-index";

    @NotNull
    private static final String DESC_PROJECT_DELETE_BY_INDEX = "удалить проект по индексу";

    @NotNull
    private static final String NOTIFY_PROJECT_DELETE_BY_INDEX =
            "Происходит попытка инициализации удаления проекта. \n" +
                    "Для удаления проекта по индексу введите индекс проекта из списка. ";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_PROJECT_DELETE_BY_INDEX;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_PROJECT_DELETE_BY_INDEX;
    }

    @Override
    public void execute() {
        @NotNull final IServiceLocatorService serviceLocator = super.locatorService.getServiceLocator();
        @NotNull final ICurrentSessionService currentSessionService = serviceLocator.getCurrentSession();
        @Nullable final Session open = currentSessionService.getSession();

        ViewUtil.print(NOTIFY_PROJECT_DELETE_BY_INDEX);
        @NotNull final Integer index = ViewUtil.getInteger() - 1;

        @NotNull final IEndpointLocatorService endpointLocator = super.locatorService.getEndpointLocator();
        @NotNull final ProjectEndpoint projectEndpoint = endpointLocator.getProjectEndpoint();
        @Nullable final Project project = projectEndpoint.deleteProjectByIndex(open, index);
        ViewUtil.print(project);
    }

}