package ru.renessans.jvschool.volkov.task.manager.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.endpoint.Session;

public interface ICurrentSessionRepository {

    @NotNull
    Session put(@NotNull final Session session);

    @Nullable
    Session get();

    Session delete();

}