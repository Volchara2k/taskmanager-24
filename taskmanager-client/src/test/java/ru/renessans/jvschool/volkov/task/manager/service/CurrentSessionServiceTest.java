package ru.renessans.jvschool.volkov.task.manager.service;

import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.renessans.jvschool.volkov.task.manager.api.repository.ICurrentSessionRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.endpoint.Session;
import ru.renessans.jvschool.volkov.task.manager.exception.empty.owner.EmptySessionException;
import ru.renessans.jvschool.volkov.task.manager.marker.NegativeImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.ServiceImplementation;
import ru.renessans.jvschool.volkov.task.manager.repository.CurrentSessionRepository;

import java.util.UUID;

public final class CurrentSessionServiceTest {

    @NotNull
    private final ICurrentSessionRepository currentSessionRepository = new CurrentSessionRepository();

    @NotNull
    private final ICurrentSessionService currentSessionService = new CurrentSessionService(currentSessionRepository);

    @Test(expected = EmptySessionException.class)
    @TestCaseName("Run testNegativeSubscribe for getHardwareData()")
    @Category({NegativeImplementation.class, ServiceImplementation.class})
    public void testNegativeSubscribe() {
        Assert.assertNotNull(this.currentSessionRepository);
        Assert.assertNotNull(this.currentSessionService);
        this.currentSessionService.subscribe(null);
    }

    @Test
    @TestCaseName("Run testGetSession for getHardwareData()")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testGetSession() {
        Assert.assertNotNull(this.currentSessionRepository);
        Assert.assertNotNull(this.currentSessionService);
    }

    @Test
    @TestCaseName("Run testSubscribe for getHardwareData()")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testSubscribe() {
        Assert.assertNotNull(this.currentSessionRepository);
        Assert.assertNotNull(this.currentSessionService);
        @NotNull final Session session = new Session();
        Assert.assertNotNull(session);
        session.setTimestamp(System.currentTimeMillis());
        session.setUserId(UUID.randomUUID().toString());
        @NotNull final Session putSession = this.currentSessionRepository.put(session);
        Assert.assertNotNull(putSession);

        @Nullable final Session getSession = this.currentSessionService.getSession();
        Assert.assertNotNull(getSession);
        Assert.assertEquals(session.getId(), getSession.getId());
        Assert.assertEquals(session.getTimestamp(), getSession.getTimestamp());
        Assert.assertEquals(session.getUserId(), getSession.getUserId());
    }

    @Test
    @TestCaseName("Run testUnsubscribe for getHardwareData()")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testUnsubscribe() {
        Assert.assertNotNull(this.currentSessionRepository);
        Assert.assertNotNull(this.currentSessionService);
        @NotNull final Session session = new Session();
        Assert.assertNotNull(session);
        session.setTimestamp(System.currentTimeMillis());
        session.setUserId(UUID.randomUUID().toString());
        @NotNull final Session putSession = this.currentSessionRepository.put(session);
        Assert.assertNotNull(putSession);

        @Nullable final Session unsubscribeSession = this.currentSessionService.unsubscribe();
        Assert.assertNotNull(unsubscribeSession);
        Assert.assertEquals(session.getId(), unsubscribeSession.getId());
        Assert.assertEquals(session.getTimestamp(), unsubscribeSession.getTimestamp());
        Assert.assertEquals(session.getUserId(), unsubscribeSession.getUserId());
    }

}