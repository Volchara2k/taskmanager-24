package ru.renessans.jvschool.volkov.task.manager.endpoint;

import junitparams.JUnitParamsRunner;
import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IEndpointLocatorRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEndpointLocatorService;
import ru.renessans.jvschool.volkov.task.manager.constant.DemoDataConst;
import ru.renessans.jvschool.volkov.task.manager.marker.IntegrationImplementation;
import ru.renessans.jvschool.volkov.task.manager.repository.EndpointLocatorRepository;
import ru.renessans.jvschool.volkov.task.manager.service.EndpointLocatorService;

@RunWith(value = JUnitParamsRunner.class)
@Category(IntegrationImplementation.class)
public final class SessionEndpointTest {

    @NotNull
    private final IEndpointLocatorRepository endpointLocatorRepository = new EndpointLocatorRepository();

    @NotNull
    private final IEndpointLocatorService endpointLocator = new EndpointLocatorService(endpointLocatorRepository);

    @NotNull
    private final SessionEndpoint sessionEndpoint = endpointLocator.getSessionEndpoint();

    @Test
    @TestCaseName("Run testOpenSession for openSession(\"{0}\",\"{1}\")")
    public void testOpenSession() {
        Assert.assertNotNull(this.endpointLocatorRepository);
        Assert.assertNotNull(this.endpointLocator);
        Assert.assertNotNull(this.sessionEndpoint);
        @NotNull final Session open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);
    }

    @Test
    @TestCaseName("Run testCloseSession for closeSession({1})")
    public void testCloseSession() {
        Assert.assertNotNull(this.endpointLocatorRepository);
        Assert.assertNotNull(this.endpointLocator);
        Assert.assertNotNull(this.sessionEndpoint);
        @NotNull final Session open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        final boolean isClosedSession = this.sessionEndpoint.closeSession(open);
        Assert.assertTrue(isClosedSession);
    }

    @Test
    @TestCaseName("Run testVerifyValidSessionState for validateSession({1})")
    public void testVerifyValidSessionState() {
        Assert.assertNotNull(this.endpointLocatorRepository);
        Assert.assertNotNull(this.endpointLocator);
        Assert.assertNotNull(this.sessionEndpoint);
        @NotNull final Session open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @NotNull final SessionValidState verifyValidSessionState = this.sessionEndpoint.verifyValidSessionState(open);
        Assert.assertNotNull(verifyValidSessionState);
        Assert.assertEquals(SessionValidState.SUCCESS, verifyValidSessionState);
    }

    @Test
    @TestCaseName("Run testVerifyValidPermissionState for verifyValidPermission(\"{0}\", \"{1}\")")
    public void testVerifyValidPermissionState( ) {
        Assert.assertNotNull(this.endpointLocatorRepository);
        Assert.assertNotNull(this.endpointLocator);
        Assert.assertNotNull(this.sessionEndpoint);
        @NotNull final Session open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @NotNull final PermissionValidState permissionValidState =
                this.sessionEndpoint.verifyValidPermissionState(open, UserRole.ADMIN);
        Assert.assertNotNull(permissionValidState);
        Assert.assertEquals(PermissionValidState.SUCCESS, permissionValidState);
    }

}